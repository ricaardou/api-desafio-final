<?php

header("Access-Control-Allow-Origin: *");

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/users','Usuario@all');
Route::post('/user/auth','Usuario@auth');
Route::post('/user/create','Usuario@create');
Route::get('/user/remove/{id}','Usuario@remove');
Route::post('/user/{id}','Usuario@update');

Route::get('/productos','Producto@all');
Route::post('/producto/create','Producto@create');
Route::get('/producto/remove/{id}','Producto@remove');
Route::post('/producto/{id}','Producto@update');