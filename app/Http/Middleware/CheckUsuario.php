<?php

namespace App\Http\Middleware;

use Closure;
use \App\Models\TTTest as Test;
use \App\Models\Contrato;

class CheckUsuario
{
    public function handle($request, Closure $next)
    {
        if(session('USUARIO')==null)return redirect('/Login');
        else{
            $route = \Route::getCurrentRoute()->getPath();
            if($route != "Inicio" && $route != "checkLogin" ){
                if($route != "Inicio/LogOut"){
                    if(checkPermisos($route) != false && session("USUARIO_PERMISOS")["ACCESO"] == 1){
                        if(session("USUARIO_PERMISOS")[checkPermisos($route)] == 1) {
                            $Contrato=Contrato::getContratoInstiucionesTabula(session("INSTITUCION")["ID"]);
                            if(isset($Contrato[0])){
                                $Contrato=$Contrato[0]["CONTRATO"];
                                if(isset($Contrato["TESTS"])){
                                    session(["TESTS_CONTRATADOS" => $Contrato["TESTS"]]);
                                }
                                else {
                                    session(["TESTS_CONTRATADOS" => []]);
                                }
                                if(isset($Contrato["BATERIAS"])){
                                    session(["BATERIAS_CONTRATADAS" => $Contrato["BATERIAS"] == "" ? [] : $Contrato["BATERIAS"]]);
                                }
                                else {
                                    session(["BATERIAS_CONTRATADAS" => []]);
                                }
                                return $next($request);
                            }
                            else return redirect('/Inicio');
                        }
                        else return redirect('/Inicio');
                    }else return redirect('/Inicio');
                }else return $next($request);
            }else return $next($request);
        }
    }
}
