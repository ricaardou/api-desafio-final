<?php

namespace App\Http\Middleware;

use Closure;

class CheckUsuarioLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('USUARIO') != null){
            return redirect('/Inicio');
        }
        else return $next($request);
    }
}
