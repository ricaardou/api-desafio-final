<?php

namespace App\Http\Middleware;

use Closure;
use \App\Models\Usuario;
use \App\Models\Institucion;
use \App\Models\Alumnos;
use \App\Models\Acceso;
use \App\Models\Contrato;
use \App\Models\TT_INSTITUCION_CONFIGURACION;

class ActualizarDatosUsuario
{
    /**
     * Middleware para actualización automatica de datos antes de acceder a cualquier route web dentro del sistema (Inicio/....)
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {       
        if(session('USUARIO')==null)return redirect('/Login');
        session(["MESSAGE_LOGIN"=>""]);
        $usuario_run = session("USUARIO_RUN");
        $id_institucion_activa = session("INSTITUCION_ID");
        $institucion = Institucion::find($id_institucion_activa);
        $route = \Route::getCurrentRoute()->getPath();
        $lista_de_instituciones = [];
        $isUsuario=false;
        $isAlumno=false;
        $isUsuario = Usuario::getUsuarioByInstitucionMiddleWare($usuario_run,$id_institucion_activa);
        $isUsuario = isset($isUsuario[0])?$isUsuario[0]:false;
        if($institucion!=null){
            session([
                "INSTITUCION"=>[
                    "ID"=>(String)$institucion->_id,
                    "NOMBRE"=>$institucion->NOMBRE,
                    "COMUNA"=>$institucion->COMUNA,
                    "DIRECTOR"=>$institucion->DIRECTOR,
                    "NOMBRE_DIRECTOR"=>$institucion->NOMBRE_DIRECTOR,
                    "CPIE"=>$institucion->CPIE,
                    "UTP"=>$institucion->UTP,
                    "NOMBRE_PROPIO"=>nombrePropio($institucion->NOMBRE),
                    "RBD"=>$institucion->RBD,
                    "LOGO"=>$institucion->LOGO,
                    "LOGO_2"=>$institucion->LOGO_2
                ],
                "INCLUDE"=>"defecto"
            ]);
        }
        $intituciones_usuario=Usuario::getInstitucionesByRun($usuario_run);
        $ids_instituciones=[];
        $y=[];
        foreach($intituciones_usuario as $x) {
            array_push($ids_instituciones, newMongoId($x["TT_INSTITUCION"]["ID_INSTITUCION"]));
            (String)$x["TT_INSTITUCION"]["ID_INSTITUCION"]==(String)$id_institucion_activa?array_unshift($y, $x):array_push($y, $x);
        }
        foreach($y as $x) {
            $aux=Institucion::find((String)$x["TT_INSTITUCION"]["ID_INSTITUCION"]);
            if($aux){
                $aux = $aux->toArray();
                $x=["INSTITUCION"=>$aux];
                $x["INSTITUCION"]["NOMBRE_PROPIO"] = nombrePropio($x["INSTITUCION"]["NOMBRE"]);
                $x["INSTITUCION"]["COMUNA_PROPIO"] = nombrePropio($x["INSTITUCION"]["COMUNA"]);
                array_push($lista_de_instituciones,$x);
            }
        }
        $intituciones_alumno = Alumnos::getUsuarioEnOtraInstitucion($ids_instituciones,$usuario_run);
        $y=[];
        foreach($intituciones_alumno as $x) {
            (String)$x["ALUMNOCURSO"]["IDINSTITUCION"]==(String)$id_institucion_activa?array_unshift($y, $x):array_push($y, $x);}
        foreach($y as $x) {
            $aux=Institucion::find((String)$x["ALUMNOCURSO"]["IDINSTITUCION"])->toArray();
            $x=["INSTITUCION"=>$aux];
            array_push($lista_de_instituciones,$x);
        }
        $PERMISOS=[];
        if($institucion!=null){
            if($isUsuario){
                $isUsuario["TT_INSTITUCION"]["PERMISO"]["ALUMNO"]=1;
                $isUsuario["TT_INSTITUCION"]["PERMISO"]["EVALUADO"]=0;
                session([
                    "USUARIO" => true,
                    "IS_ALUMNO" => false,
                    "USUARIO_ID"=>(String)$isUsuario["_id"],
                    "USUARIO_NOMBRE"=>$institucion->RBD==$usuario_run?nombrePropio($institucion->NOMBRE):nombrePropio(explode(" ",$isUsuario["NOMBRES"])[0]." ".$isUsuario["APELLIDOPATERNO"]),
                    "USUARIO_NOMBRE_COMPLETO"=>$institucion->RBD==$usuario_run?nombrePropio($institucion->NOMBRE):nombrePropio($isUsuario["NOMBRES"]." ".$isUsuario["APELLIDOPATERNO"]),
                    "USUARIO_NOMBRE_2"=>$institucion->RBD==$usuario_run?nombrePropio($institucion->NOMBRE):nombrePropio(explode(" ",$isUsuario["NOMBRES"])[0]." ".$isUsuario["APELLIDOPATERNO"]." ".$isUsuario["APELLIDOMATERNO"]),
                    "USUARIO_IMAGEN"=>$isUsuario["IMAGEN"],
                    "USUARIO_PROFESION"=>($isUsuario["PROFESION"]),
                    "USUARIO_TELEFONO"=>trim($isUsuario["TELEFONO"]),
                    "USUARIO_CORREO"=>trim($isUsuario["CORREO"]),
                    "USUARIO_REGISTRO"=>isset($isUsuario["NREGISTRO"])?trim($isUsuario["NREGISTRO"]):'',
                    "USUARIO_TIPOUSUARIO"=>(int)$isUsuario["TT_INSTITUCION"]["ID_TIPOUSUARIO"],            
                    "USUARIO_PERMISOS"=>$isUsuario["TT_INSTITUCION"]["PERMISO"],
                    "TABULAPIE"=>false,
                    "USUARIO_RUN"=>$usuario_run,
                    "INCLUDE"=>"defecto"
                ]);
                if(session("FIRMA")==null){
                    session([
                        "FIRMA"=>[
                            "NOMBRE"=>$institucion->RBD==$usuario_run?nombrePropio($institucion->NOMBRE):nombrePropio(explode(" ",$isUsuario["NOMBRES"])[0]." ".$isUsuario["APELLIDOPATERNO"]),
                            "CARGO"=>nombrePropio($isUsuario["PROFESION"])
                        ]
                    ]);
                }
                $PERMISOS=isset($isUsuario["TT_INSTITUCION"]["PERMISO"])?$isUsuario["TT_INSTITUCION"]["PERMISO"]:[];
            }
            $isAlumno = Alumnos::getUsuario((String)$institucion->_id,$usuario_run);
            $isAlumno = $isAlumno!=null?$isAlumno:false;
            if($isAlumno){
                if($isUsuario){
                    $isUsuario["TT_INSTITUCION"]["PERMISO"]["EVALUADO"]=1;
                }
                else if(!$isUsuario){
                    session([
                        "USUARIO" => true,
                        "IS_ALUMNO" => true,
                        "USUARIO_RUN"=>$usuario_run,
                        "USUARIO_ID"=>(String)$isAlumno["_id"],
                        "USUARIO_NOMBRE"=>nombrePropio(explode(" ",$isAlumno["NOMBRES"])[0]." ".$isAlumno["APELLIDOPATERNO"]),
                        "USUARIO_NOMBRE_2"=>nombrePropio(explode(" ",$isAlumno["NOMBRES"])[0]." ".$isAlumno["APELLIDOPATERNO"]." ".$isAlumno["APELLIDOMATERNO"]),
                        "USUARIO_IMAGEN"=>$isAlumno["IMAGEN"],
                        "USUARIO_PROFESION"=>nombrePropio($isAlumno["ALUMNOCURSO"]["CURSO"])!="Funcionarios"?$isAlumno["ALUMNOCURSO"]["CURSO"]." ".$isAlumno["ALUMNOCURSO"]["DETALLE"]:$isAlumno["ALUMNOCURSO"]["CURSO"]." ".nombrePropio($isAlumno["ALUMNOCURSO"]["DETALLE"]),
                        "USUARIO_TIPOUSUARIO"=>6,            
                        "TABULAPIE"=>false,
                        "USUARIO_PERMISOS"=>['TABULAPIE'=>0, 'MEDICION' => 0, 'INFORME' => 0, "CURSOS" => 0, "ALUMNOS" => 0, "ACCESO" => 1, "CONFIGURACION" => 0,"JUEGOS"=>0, "ALUMNO"=>1, "EVALUADO" => 1,"MODULO_KL" => 1,"MODULO_SM"=>1,"MODULO_LP"=>1,"ESTRATEGIA"=>1,"MODULO_MS" => 1,"MODULO_MB"=>1,"MODULO_LG"=>1],
                        "INCLUDE"=>"alumno"
                    ]);
                    $PERMISOS = ['TABULAPIE'=>0, 'MEDICION' => 0, 'INFORME' => 0, "CURSOS" => 0, "ALUMNOS" => 0, "ACCESO" => 1, "CONFIGURACION" => 0,"JUEGOS"=>0, "ALUMNO"=>1, "EVALUADO" => 1,"MODULO_KL" => 1,"MODULO_SM"=>1,"MODULO_LP"=>1,"ESTRATEGIA"=>1,"MODULO_MS" => 1,"MODULO_MB"=>1,"MODULO_LG"=>1];
                }
                session([
                    "ALUMNO_ACTUAL"=>$isAlumno,
                    "ALUMNOCURSO"=>$isAlumno["ALUMNOCURSO"]
                ]);
            }
        }
        if($isUsuario){
            if (!isset($PERMISOS['TABULAPIE'])&&$isUsuario)Usuario::SetPermiso($id_institucion_activa,(String)$isUsuario["_id"],'TABULAPIE',1);
            if (!isset($PERMISOS['TABULAPIE'])&&$isUsuario) $PERMISOS['TABULAPIE']=1;

            if (!isset($PERMISOS['COORDINADOR_PIE'])&&$isUsuario)Usuario::SetPermiso($id_institucion_activa,(String)$isUsuario["_id"],'COORDINADOR_PIE',0);
            if (!isset($PERMISOS['COORDINADOR_PIE'])&&$isUsuario) $PERMISOS['COORDINADOR_PIE']=0;
        }
        if (!isset($PERMISOS['TABULAPIE'])&&$isUsuario) $PERMISOS['TABULAPIE']=1;
        session([ "USUARIO_PERMISOS" => $PERMISOS ]);
        if($institucion==null&&isset($lista_de_instituciones[0])){
            Acceso::setInstitucionDefecto($usuario_run,(String)$lista_de_instituciones[0]["INSTITUCION"]["_id"]);
            session(["INSTITUCION_ID"=>(String)$lista_de_instituciones[0]["INSTITUCION"]["_id"]]);
            return redirect($route);
        }
        //Chequea Contrato de Instituciones del usuario
        $lista_de_instituciones_aux=$lista_de_instituciones;$lista_de_instituciones=[];
        foreach($lista_de_instituciones_aux as $x) {
            $hasContrato=Contrato::getContratoInstiucionesTabulaExistente((String)$x["INSTITUCION"]["_id"]);
            $hasContrato = isset($hasContrato[0])?$hasContrato[0]:false;
            if($hasContrato){
                if((String)$x["INSTITUCION"]["_id"]!=$id_institucion_activa){
                    array_push($lista_de_instituciones, $x);
                }
            }
        }
        //Chequea Contrato de Insitucion Actual
        $hasContrato=Contrato::getContratoInstiucionesTabulaExistente($id_institucion_activa);
        $hasContrato = isset($hasContrato[0])?$hasContrato[0]:false;
        $BloqueoCursos = [];
        $ConfigInstitucion = TT_INSTITUCION_CONFIGURACION::where("ID_INSTITUCION",newMongoId($id_institucion_activa))->get()->toArray();
        session(["ETAPAS"=>[]]);
        if($hasContrato){
            $hasContrato_Aux = $hasContrato; 
            $PERMISOS = session("USUARIO_PERMISOS");
            

            if (isset($hasContrato_Aux["CONTRATO"]["CLASIFICACION_BATERIAS"])) {
                $PERMISOS["CLASIFICACION_BATERIAS"]=$hasContrato_Aux["CONTRATO"]["CLASIFICACION_BATERIAS"];
            }else{
                $PERMISOS["CLASIFICACION_BATERIAS"]=false;
            }

            if (isset($hasContrato_Aux["CONTRATO"]["MODULO_KL"])) {
                $PERMISOS["MODULO_KL"]=$hasContrato_Aux["CONTRATO"]["MODULO_KL"];
            }else{
                $PERMISOS["MODULO_KL"]=1;
                $hasContrato_Aux["CONTRATO"]["MODULO_KL"] = 1;
            }
            if (isset($hasContrato_Aux["CONTRATO"]["MODULO_SM"])) {
                $PERMISOS["MODULO_SM"]=$hasContrato_Aux["CONTRATO"]["MODULO_SM"];
            }else{
                $PERMISOS["MODULO_SM"]=1;
                $hasContrato_Aux["CONTRATO"]["MODULO_SM"] = 1;
            }
            if (isset($hasContrato_Aux["CONTRATO"]["MODULO_LP"])) {
                $PERMISOS["MODULO_LP"]=$hasContrato_Aux["CONTRATO"]["MODULO_LP"];
            }else{
                $PERMISOS["MODULO_LP"]=1;
                $hasContrato_Aux["CONTRATO"]["MODULO_LP"] = 1;
            }
            if (isset($hasContrato_Aux["CONTRATO"]["MODULO_MS"])) {
                $PERMISOS["MODULO_MS"]=$hasContrato_Aux["CONTRATO"]["MODULO_MS"];
            }else{
                $PERMISOS["MODULO_MS"]=1;
                $hasContrato_Aux["CONTRATO"]["MODULO_MS"] = 1;
            }
            if (isset($hasContrato_Aux["CONTRATO"]["MODULO_MB"])) {
                $PERMISOS["MODULO_MB"]=$hasContrato_Aux["CONTRATO"]["MODULO_MB"];
            }else{
                $PERMISOS["MODULO_MB"]=1;
                $hasContrato_Aux["CONTRATO"]["MODULO_MB"] = 1;
            }
            if (isset($hasContrato_Aux["CONTRATO"]["MODULO_LG"])) {
                $PERMISOS["MODULO_LG"]=$hasContrato_Aux["CONTRATO"]["MODULO_LG"];
            }else{
                $PERMISOS["MODULO_LG"]=1;
                $hasContrato_Aux["CONTRATO"]["MODULO_LG"] = 1;
            }
            if (isset($hasContrato_Aux["CONTRATO"]["ESTRATEGIA"])) {
                $PERMISOS["ESTRATEGIA"]=$hasContrato_Aux["CONTRATO"]["ESTRATEGIA"];
                if ($PERMISOS["ESTRATEGIA"] == 0) {
                    $PERMISOS["MODULO_KL"]=0;
                    $PERMISOS["MODULO_SM"]=0;
                    $PERMISOS["MODULO_LP"]=0;
                    $PERMISOS["MODULO_MS"]=0;
                    $PERMISOS["MODULO_MB"]=0;
                    $PERMISOS["MODULO_LG"]=0;
                }else{
                    $PERMISOS["MODULO_KL"]=$hasContrato_Aux["CONTRATO"]["MODULO_KL"];
                    $PERMISOS["MODULO_SM"]=$hasContrato_Aux["CONTRATO"]["MODULO_SM"];
                    $PERMISOS["MODULO_LP"]=$hasContrato_Aux["CONTRATO"]["MODULO_LP"];
                    $PERMISOS["MODULO_MS"]=$hasContrato_Aux["CONTRATO"]["MODULO_MS"];
                    $PERMISOS["MODULO_MB"]=$hasContrato_Aux["CONTRATO"]["MODULO_MB"];
                    $PERMISOS["MODULO_LG"]=$hasContrato_Aux["CONTRATO"]["MODULO_LG"];
                }
            }else{
                $PERMISOS["ESTRATEGIA"]=1;
                $hasContrato_Aux["CONTRATO"]["ESTRATEGIA"] = 1;
            }   
            if(isset($hasContrato_Aux["CONTRATO"]["BATERIAS"])){
                if(count($hasContrato_Aux["CONTRATO"]["BATERIAS"])>0){
                    //$hasContrato_Aux = MongoToArray([$hasContrato],[],["BATERIAS"],["CONTRATO"])[0];
                    $PERMISOS["TABULAPIE"]=isset($PERMISOS["TABULAPIE"])?$PERMISOS["TABULAPIE"]:1;
                    $ETAPAS = isset($hasContrato_Aux["CONTRATO"]["ETAPAS"])?$hasContrato_Aux["CONTRATO"]["ETAPAS"]:[];
                    session(["ETAPAS"=>$ETAPAS]);
                    session(["TABULAPIE"=>true]);
                    session(["FECHATERMINO" => $hasContrato["CONTRATO"]["FECHATERMINO"]]);
                }
                else{
                    $PERMISOS["TABULAPIE"]=0;
                    session(["TABULAPIE"=>false]);
                }
            }
            else{
                $PERMISOS["TABULAPIE"]=0;
                session(["TABULAPIE"=>false]);
            }
        }
        else{
            $PERMISOS["ESTRATEGIA"]=0;
            $PERMISOS["CONFIGURACION"]=0;
        }
        session([ "USUARIO_PERMISOS" => $PERMISOS ]);
        if(isset($ConfigInstitucion[0])){
            if(isset($ConfigInstitucion[0]["BLOQUEO_CURSOS"]))
                $BloqueoCursos = $ConfigInstitucion[0]["BLOQUEO_CURSOS"];
        }
        session([ "CONFIGURACION_BLOQUEO_CURSOS" => $BloqueoCursos ]);
        session([ "USUARIO_LISTA_INSTITUCIONES" => array_values($lista_de_instituciones) ]);
        if(!$isAlumno&&!$isUsuario||!$hasContrato){
            if($request->ajax()){
                session(["ERROR_INSITUCION"=>true]);
                return $next($request);
            }
            if(!isset($lista_de_instituciones[0])){
                $msg="";
                if(!$hasContrato){
                    if($isUsuario){
                        $msg='Estimado(a) Profesor(a). La licencia anual de "Tabulatest" para su colegio ha caducado. Gracias por utilizar "Tabulatest"!!!';
                    }
                    else if($isAlumno){
                        if($isAlumno["DOCENTE"]){
                            $msg='Estimado(a) Profesor(a). La licencia anual de "Tabulatest" para su colegio ha caducado. Gracias por utilizar "Tabulatest"!!!';
                        }
                        else{
                            $msg='Estimado(a) Alumno(a). La licencia de "Tabulatest" para tu colegio ha caducado. Contacta a tu profesor(a) o Director(a) para consultar más información. ¡Gracias por usar "Tabulatest" !!';
                        }
                    }
                }
                else{
                    $msg="El usuario ingresado no se encuentra activo en ninguna institución.";
                }
                session(["USUARIO" => null, "MESSAGE_LOGIN" => $msg]);
                //return $next($request);
                return redirect('/Login');
            }
            session(["INSTITUCION_ID"=>(String)$lista_de_instituciones[0]["INSTITUCION"]["_id"]]);
            return redirect($route);
        }
        // Deja la institucion como por defecto
        Acceso::setInstitucionDefecto($usuario_run,$id_institucion_activa);
        return $next($request);
    }
}