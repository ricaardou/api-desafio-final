<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/git/pull/master',
        '/git/pull/production',
        '/git/pull/mantenedores',
		'/upload/image/from/mantenedor/',
        '/UploadFileToServerExtensionChrome'
    ];
}
