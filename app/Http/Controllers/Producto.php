<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller as ControladorBase;
use Illuminate\Http\Request;
    use \Throwable as Exception;
    use App\Models\PRODUCTOS;

class Producto extends ControladorBase
{
    public function index(){}

    public function create(Request $Data){
        return json_encode(PRODUCTOS::create($Data->input("DATA")));
    }
    public function update($id, Request $Data){
        return json_encode(PRODUCTOS::modify($id,$Data->input("DATA")));
    }
    public function all(Request $Data){
        return json_encode(PRODUCTOS::all()->toArray());
    }
    public function remove($id){
        return json_encode(PRODUCTOS::find($id)->delete());
    }
}