<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller as ControladorBase;
use Illuminate\Http\Request;
    use \Throwable as Exception;
    use App\Models\USUARIOS;

class Usuario extends ControladorBase
{
    public function index(){}

    public function auth(Request $Data){
        return json_encode(USUARIOS::auth($Data->input("DATA")));
    }
    public function create(Request $Data){
        return json_encode(USUARIOS::create($Data->input("DATA")));
    }
    public function update($id, Request $Data){
        return json_encode(USUARIOS::modify($id,$Data->input("DATA")));
    }
    public function all(Request $Data){
        return json_encode(USUARIOS::all()->toArray());
    }
    public function remove($id){
        return json_encode(USUARIOS::find($id)->delete());
    }
}