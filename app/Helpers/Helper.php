<?php
if (!function_exists('VersionArchivosJsPrincipales')) {
  function VersionArchivosJsPrincipales(){
    return '4.14.32';
  }
}

if (!function_exists('getPromocion')) {
  function getPromocion(){
    return (int)session("PROMOCION");
  }
}
if (!function_exists('str_replace_first')) {
  function str_replace_first($from, $to, $content)
  {
      $from = '/'.preg_quote($from, '/').'/';
      return preg_replace($from, $to, $content, 1);
  }
}

if (!function_exists('crear_registro_auditoria')) {
  function crear_registro_auditoria($MODULO, $ACCION, $DETALLE)
  {
      \App\Models\TT_AUDITORIA::crear_registro($MODULO, $ACCION, $DETALLE, \Route::getCurrentRoute()->getPath());
  }
}

if(! function_exists('tipoDeFormulario')){     
    function tipoDeFormulario($formulario){
    if($formulario == "TEL" || $formulario == "DEA" || $formulario == "TDA" || $formulario == "NEET" || $formulario == "NEEP" || $formulario == "DISCAPACIDAD_MOTORA" || $formulario == "DISCAPACIDAD_VISUAL" || $formulario == "DISCAPACIDAD_AUDITIVA" || $formulario == "DISCAPACIDAD_MULTIPLE" || $formulario == "TEA" || $formulario == "DISFASIA_SEVERA")
      return "FU";
    else if ($formulario == "VALORACION_SALUD")
      return "VALORACION_SALUD";    
    else return "OTRO";    
    }
}

if (!function_exists('upadte_xml_phpword')) {
  function upadte_xml_phpword($XML, $_Array=[]){
    $XML = explode("<w:ffData>", $XML);
    $newStringXML = "";
    $contador = 0;

    


    $_Array = session("WORD-GENERATE");
    foreach ($XML as $key => $row) {
        if($key!==0){
            $newStringXML.="<w:ffData>";
            
            $textInput = strpos($row, "<w:textInput>");
            $checkBox  = strpos($row, "<w:checkBox>");
            $value = isset( $_Array[$contador] ) ? $_Array[$contador]["value"] : false;
            if($textInput === false){
                if ($checkBox === false){} 
                else{
                    $contador++;
                    $newStringXML.= str_replace(
                        '<w:checkBox>', 
                        '<w:checkBox><w:checked w:val="'.($value===false?"false":$value).'"/>', 
                        str_replace(
                            '<w:checked w:val="0"/>', 
                            '', 
                            $row
                        )
                    );
                }
            } 
            else{
                // if($_Array[$contador]["type"] == "date" )
                //   $value =  str_replace('-', '/', $value );

                $contador++;
                //$value=str_replace(PHP_EOL, "<w:br />", $value);
                $value=str_replace(">", "&gt;", $value);
                $value=str_replace("<", "&lt;", $value);

                $row_ =  str_replace_first('<w:t>', '<w:t>'.($value===false?" noexiste ":($value."-@@@-")), $row );
                $row_ =  str_replace(' -@@@-', '-@@@-', $row_ );
                $row_ =  str_replace('-@@@-', '', $row_ );
                for ($f=0; $f < 30; $f++) { 
                    $row_ =  str_replace('<w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="'.$f.'"/><w:szCs w:val="'.$f.'"/></w:rPr><w:t> </w:t>', '', $row_ );  
                    $row_ =  str_replace('<w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:sz w:val="'.$f.'"/><w:szCs w:val="'.$f.'"/></w:rPr><w:t> </w:t>', '', $row_ );  
                    $row_ =  str_replace('<w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="'.$f.'"/><w:szCs w:val="'.$f.'"/></w:rPr><w:t> </w:t>', '', $row_ );  
                    $row_ =  str_replace('<w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:noProof/><w:sz w:val="'.$f.'"/><w:szCs w:val="'.$f.'"/></w:rPr><w:t> </w:t>', '', $row_ );  
                }
                
                $newStringXML.=$row_;
                


            }
        }
        else{
            $newStringXML=$row;
        }
    }
    foreach (session("SPECIAL_REPLACES") as $key => $value) {
      $newStringXML = str_replace($key, $value, $newStringXML);
    }
    $newStringXML = str_replace('</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);
      
      
      $newStringXML = str_replace(' </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:fldChar w:fldCharType="end"/></w:r></w:p>', 
        '@</w:t></w:r>@<w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:fldChar w:fldCharType="end"/></w:r></w:p>', $newStringXML);

      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="005D665A"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="005D665A"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="005D665A"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="005D665A"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      //TEL
      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="000520BD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="000520BD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="000520BD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="000520BD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      //DEA
      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00A53559"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00A53559"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00A53559"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00A53559"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      //TDA
      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00B51871"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00B51871"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00B51871"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00B51871"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      //TDA
      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="000937CD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="000937CD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="000937CD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="000937CD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      

      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00D6242D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00D6242D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00D6242D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00D6242D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="008D7A44"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="008D7A44"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="008D7A44"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="008D7A44"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="007C40D3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="007C40D3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="007C40D3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="007C40D3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="007C40D3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="007C40D3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="007C40D3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="007C40D3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="008D7A44"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="008D7A44"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="008D7A44"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="008D7A44"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      


      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00696C2D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00696C2D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00696C2D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00696C2D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);


      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00397933"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00397933"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00397933"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00397933"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="004470BF"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="004470BF"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="004470BF"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="004470BF"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00C844C1"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00C844C1"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00C844C1"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00C844C1"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);


      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00B8760E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00B8760E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00B8760E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00B8760E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      
      $newStringXML = str_replace('</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);
        


      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="002E6A37"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="002E6A37"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="002E6A37"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="002E6A37"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="0091747F"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="0091747F"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="0091747F"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="0091747F"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);




     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00DB52FE"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00DB52FE"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00DB52FE"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00DB52FE"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);


     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00E91D55"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00E91D55"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00E91D55"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00E91D55"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);


     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00671FCA"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00671FCA"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00671FCA"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00671FCA"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="002D65BC"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="002D65BC"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="002D65BC"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="002D65BC"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00AE65E7"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00AE65E7"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00AE65E7"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00AE65E7"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);


     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00673295"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00673295"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00673295"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00673295"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);


     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00095921"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00095921"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00095921"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00095921"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);


     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidR="009D6023"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="009D6023"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="009D6023"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="009D6023"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidR="007A72C7" w:rsidRPr="00B162FD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="007A72C7" w:rsidRPr="00B162FD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="007A72C7" w:rsidRPr="00B162FD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="007A72C7" w:rsidRPr="00B162FD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

      $newStringXML = str_replace('</w:t></w:r><w:r w:rsidR="007A72C7" w:rsidRPr="00B162FD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="007A72C7" w:rsidRPr="00B162FD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="007A72C7" w:rsidRPr="00B162FD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="007A72C7" w:rsidRPr="00B162FD"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidR="00637D4D" w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00637D4D" w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00637D4D" w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00637D4D" w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);


     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidR="00637D4D" w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00637D4D" w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00637D4D" w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00637D4D" w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="0019274C"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidRPr="00827D6E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00827D6E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00827D6E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00827D6E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);


     $newStringXML = str_replace('</w:t></w:r><w:r w:rsidR="00EE66B0"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00EE66B0"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00EE66B0"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00EE66B0"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow" w:cs="Arial"/><w:bCs/><w:noProof/><w:color w:val="000000"/><w:sz w:val="22"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace('</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/></w:rPr><w:t> </w:t></w:r>', '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace(['</w:t></w:r><w:r w:rsidR="002A3F8E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002A3F8E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002A3F8E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002A3F8E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '</w:t></w:r><w:r w:rsidRPr="00C16B9E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00C16B9E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00C16B9E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00C16B9E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r>', '</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r>', '</w:t></w:r><w:r w:rsidR="002A3F8E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002A3F8E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002A3F8E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002A3F8E"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="16"/></w:rPr><w:t> </w:t></w:r>', '</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:eastAsia="Times New Roman" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="18"/></w:rPr><w:t> </w:t></w:r>', '</w:t></w:r><w:r w:rsidRPr="005A75B3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="005A75B3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="005A75B3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="005A75B3"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>', '</w:t></w:r><w:r w:rsidR="00836CC8"></w:r><w:r w:rsidR="00836CC8"></w:r><w:r w:rsidR="00836CC8"></w:r><w:r w:rsidR="00836CC8"></w:r>'], '@</w:t></w:r>@', $newStringXML);

     $newStringXML = str_replace([
      '</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="22"/></w:rPr><w:t> </w:t></w:r>',
      '</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>',
      '</w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>',
      '</w:t></w:r><w:r w:rsidR="00047978"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00047978"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00047978"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00047978"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>',
      '</w:t></w:r><w:r w:rsidR="00AE65E7"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00AE65E7"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00AE65E7"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00AE65E7"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>',
      '</w:t></w:r><w:r w:rsidR="00047978"></w:r><w:r w:rsidR="00047978"></w:r><w:r w:rsidR="00047978"></w:r><w:r w:rsidR="00047978"></w:r>'
        ], '@</w:t></w:r>@', $newStringXML);

       $newStringXML = str_replace(['</w:t></w:r><w:r w:rsidRPr="00D838E0"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00D838E0"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00D838E0"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="00D838E0"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:b/><w:noProof/><w:sz w:val="18"/><w:szCs w:val="20"/></w:rPr><w:t> </w:t></w:r>'], '@</w:t></w:r>@', $newStringXML);
     
       $newStringXML = str_replace(['</w:t></w:r><w:r w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r>','</w:t></w:r><w:r w:rsidR="002844E6" w:rsidRPr="002844E6"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002844E6" w:rsidRPr="002844E6"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002844E6" w:rsidRPr="002844E6"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002844E6" w:rsidRPr="002844E6"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r>','</w:t></w:r><w:r w:rsidRPr="002844E6"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="002844E6"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="002844E6"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidRPr="002844E6"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r>','</w:t></w:r><w:r w:rsidR="00461248" w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00461248" w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00461248" w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="00461248" w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r>','</w:t></w:r><w:r w:rsidR="002D6BB5" w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002D6BB5" w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002D6BB5" w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r><w:r w:rsidR="002D6BB5" w:rsidRPr="001B199D"><w:rPr><w:rFonts w:ascii="Arial Narrow" w:hAnsi="Arial Narrow"/><w:noProof/><w:sz w:val="20"/><w:szCs w:val="20"/><w:highlight w:val="lightGray"/></w:rPr><w:t> </w:t></w:r>'], '@</w:t></w:r>@', $newStringXML);


  $newStringXML = str_replace('</w:t></w:r><w:r></w:r><w:r></w:r><w:r></w:r><w:r></w:r>', '@</w:t></w:r><w:r></w:r><w:r></w:r><w:r></w:r><w:r></w:r>@', $newStringXML);     


      $newStringXML = str_replace(' @</w:t></w:r><w:r></w:r><w:r></w:r><w:r></w:r><w:r></w:r>@', '</w:t></w:r><w:r></w:r><w:r></w:r><w:r></w:r><w:r></w:r>', $newStringXML);
      $newStringXML = str_replace('@</w:t></w:r><w:r></w:r><w:r></w:r><w:r></w:r><w:r></w:r>@', '</w:t></w:r><w:r></w:r><w:r></w:r><w:r></w:r><w:r></w:r>', $newStringXML);
      $newStringXML = str_replace(' @</w:t></w:r>@', '</w:t></w:r>', $newStringXML);
      $newStringXML = str_replace('@</w:t></w:r>@', '</w:t></w:r>', $newStringXML);
      $newStringXML = str_replace(' #</w:t></w:r>#', '</w:t></w:r>', $newStringXML);
      $newStringXML = str_replace('#</w:t></w:r>#', '</w:t></w:r>', $newStringXML);
      
     //dd($newStringXML);

    return $newStringXML;
  }
}
/*** BACKUP **/
if (!function_exists('UploadFileTabulatest')) {
  function UploadFileTabulatest($Registros){
    foreach($Registros as $Index => $Row){
      if($Row["IMAGEN"]!="IMAGEN" && $Row["IMAGEN"]!="false" && $Row["IMAGEN"]!=""){
        $Http = strpos($Row["IMAGEN"], "http://");
        $Https = strpos($Row["IMAGEN"], "https://");
        if ($Http === false && $Https === false){}
        else{
          $Contents = $Row["IMAGEN"];
          $Basename = basename($Row["IMAGEN"]);
          file_put_contents(public_path("assets/images/tabulatest/".$Basename), fopen($Contents, 'r'));
          $Row->IMAGEN = url("assets/images/tabulatest/".$Basename);
          $Row->save();

        }
      }
    }
  }
}
if(!function_exists('getEdadWisc')){
  function getEdadWisc($date,$fecha=""){
    //return getEdad2($date,$fecha);
    if($fecha == ""){
      $fecha = date("Y-m-d");
    }
    $date = date('Y-m-d', strtotime($date));
    $fecha_de_nacimiento = $date;
    $fecha_actual = $fecha;
    $array_nacimiento = explode ( "-", $fecha_de_nacimiento );
    $array_actual = explode ( "-", $fecha_actual );

    $anos =  $array_actual[0] - $array_nacimiento[0];
    $meses = $array_actual[1] - $array_nacimiento[1];
    $dias =  $array_actual[2] - $array_nacimiento[2];
    if ($dias < 0)
    {
      --$meses;
      switch ($array_actual[1]) {
        case 1:     $dias_mes_anterior=30; break;
        case 2:     $dias_mes_anterior=30; break;
        case 3:
          if (bisiesto($array_actual[0]))
          {
            $dias_mes_anterior=30; break;
          } else {
            $dias_mes_anterior=30; break;
          }
        case 4:     $dias_mes_anterior=30; break;
        case 5:     $dias_mes_anterior=30; break;
        case 6:     $dias_mes_anterior=30; break;
        case 7:     $dias_mes_anterior=30; break;
        case 8:     $dias_mes_anterior=30; break;
        case 9:     $dias_mes_anterior=30; break;
        case 10:     $dias_mes_anterior=30; break;
        case 11:     $dias_mes_anterior=30; break;
        case 12:     $dias_mes_anterior=30; break;
      }

      $dias=$dias + $dias_mes_anterior;
    }
    if ($meses < 0)
    {
      --$anos;
      $meses=$meses + 12;
    }
    return array( "y" => $anos, "m" => $meses, "d" => $dias);
  }
  // function bisiesto($anio_actual){
  // $bisiesto=false;
  //   if (checkdate(2,29,$anio_actual))
  //   {
  //   $bisiesto=true;
  //   return $bisiesto;
  //    }
  // }
}
if(!function_exists('getEdad')){
  function getEdad($date,$fecha=""){
    if($fecha == ""){
      $fecha = date("Y-m-d");
    }
    $date = date('Y-m-d', strtotime($date));
    $fecha_de_nacimiento = $date;
    $fecha_actual = $fecha;
    $array_nacimiento = explode ( "-", $fecha_de_nacimiento );
    $array_actual = explode ( "-", $fecha_actual );

    $anos =  $array_actual[0] - $array_nacimiento[0];
    $meses = $array_actual[1] - $array_nacimiento[1];
    $dias =  $array_actual[2] - $array_nacimiento[2];
    if ($dias < 0)
    {
      --$meses;
      switch ($array_actual[1]) {
        case 1:     $dias_mes_anterior=31; break;
        case 2:     $dias_mes_anterior=31; break;
        case 3:
          if (bisiesto($array_actual[0]))
          {
            $dias_mes_anterior=29; break;
          } else {
            $dias_mes_anterior=28; break;
          }
        case 4:     $dias_mes_anterior=31; break;
        case 5:     $dias_mes_anterior=30; break;
        case 6:     $dias_mes_anterior=31; break;
        case 7:     $dias_mes_anterior=30; break;
        case 8:     $dias_mes_anterior=31; break;
        case 9:     $dias_mes_anterior=31; break;
        case 10:     $dias_mes_anterior=30; break;
        case 11:     $dias_mes_anterior=31; break;
        case 12:     $dias_mes_anterior=30; break;
      }

      $dias=$dias + $dias_mes_anterior;
    }
    if ($meses < 0)
    {
      --$anos;
      $meses=$meses + 12;
    }
    return array( "y" => $anos, "m" => $meses, "d" => $dias);
  }
  function bisiesto($anio_actual){
  $bisiesto=false;
    if (checkdate(2,29,$anio_actual))
    {
    $bisiesto=true;
    return $bisiesto;
     }
  }
}
if(!function_exists('getEdad2')){
  function getEdad2($date,$fecha_us){
    if($fecha_us == ""){
      $fecha_us = date("Y-m-d");
    }
    $date = date('Y-m-d', strtotime($date));
    //date("oW", strtotime("2011-12-31"))
    $fecha_de_nacimiento = $date;
    $fecha_actual = $fecha_us;
    $array_nacimiento = explode ( "-", $fecha_de_nacimiento );
    $array_actual = explode ( "-", $fecha_actual );

    $anos =  $array_actual[0] - $array_nacimiento[0];
    $meses = $array_actual[1] - $array_nacimiento[1];
    $dias =  $array_actual[2] - $array_nacimiento[2];
    if ($dias < 0)
    {
      --$meses;
      switch ($array_actual[1]) {
        case 1:     $dias_mes_anterior=31; break;
        case 2:     $dias_mes_anterior=31; break;
        case 3:
          if (bisiesto2($array_actual[0]))
          {
            $dias_mes_anterior=29; break;
          } else {
            $dias_mes_anterior=28; break;
          }
        case 4:     $dias_mes_anterior=31; break;
        case 5:     $dias_mes_anterior=30; break;
        case 6:     $dias_mes_anterior=31; break;
        case 7:     $dias_mes_anterior=30; break;
        case 8:     $dias_mes_anterior=31; break;
        case 9:     $dias_mes_anterior=31; break;
        case 10:     $dias_mes_anterior=30; break;
        case 11:     $dias_mes_anterior=31; break;
        case 12:     $dias_mes_anterior=30; break;
      }

      $dias=$dias + $dias_mes_anterior;
    }
    if ($meses < 0)
    {
      --$anos;
      $meses=$meses + 12;
    }
    return array( "y" => $anos, "m" => $meses, "d" => $dias);
  }
  function bisiesto2($anio_actual){
  $bisiesto=false;
    if (checkdate(2,29,$anio_actual))
    {
    $bisiesto=true;
    return $bisiesto;
     }
  }
}
if (!function_exists('BackUp_Database')) {
  function BackUp_Database($ModelName,$Message){
    $Contador = 0;
    $Modelo = $ModelName::on("cloud")->get();
    foreach ($Modelo as $Index => $Row) {
      $NewArray = [];
      foreach($Row->getAttributes() as $Atributo => $Valor) $NewArray[$Atributo] = $Row[$Atributo];
      unset($NewArray["_id"]);unset($NewArray["updated_at"]);unset($NewArray["created_at"]);unset($NewArray["updatedAt"]);
      $ModelName::on("general")->where("_id",$Row->_id)->update($NewArray,["upsert"=>true]);
      $Contador++;
    }
    echo $Contador." Registros de <b>".$Message."</b><br>";
  }
}
if (!function_exists('BackUp_Database_Institucion')) {
  function BackUp_Database_Institucion($ModelName,$Message,$Institucion,$IDINSTITUCION){
    $Contador = 0;
    $Modelo = $ModelName::on("cloud")->where($IDINSTITUCION,newMongoId((String)$Institucion))->get();
    foreach ($Modelo as $Index => $Row) {
      $NewArray = [];
      foreach($Row->getAttributes() as $Atributo => $Valor) $NewArray[$Atributo] = $Row[$Atributo];
      unset($NewArray["_id"]);unset($NewArray["updated_at"]);unset($NewArray["created_at"]);unset($NewArray["updatedAt"]);
      $ModelName::on("general")->where("_id",$Row->_id)->update($NewArray,["upsert"=>true]);
      $Contador++;
    }
    echo $Contador." Registros de <b>".$Message."</b><br>";
  }
}

if (!function_exists('BackUp_Database_Acceso')) {
  function BackUp_Database_Acceso($ModelName,$Message){
    $Contador = 0;
    $Modelo = $ModelName::on("cloud")->where("LT_INSTITUCION.TIPO","<>","6")->where("MT_INSTITUCION.TIPO","<>","6")->where("TT_INSTITUCION.TIPO","<>","6")->get();
    foreach ($Modelo as $Index => $Row) {
      $NewArray = [];
      foreach($Row->getAttributes() as $Atributo => $Valor) $NewArray[$Atributo] = $Row[$Atributo];
      unset($NewArray["_id"]);unset($NewArray["updated_at"]);unset($NewArray["created_at"]);unset($NewArray["updatedAt"]);
      $ModelName::on("general")->where("_id",$Row->_id)->update($NewArray,["upsert"=>true]);
      $Contador++;
    }
    echo $Contador." Registros de <b>".$Message."</b><br>";
  }
}
if (!function_exists('BackUp_Database_Contrato')) {
  function BackUp_Database_Contrato($ModelName, $Message, $Institucion){
    $Contador = 0;
    $Modelo = $ModelName::on("cloud")->where("IDINSTITUCION",newMongoId($Institucion))->get();
    foreach ($Modelo as $Index => $Row) {
      $NewArray = [];
      foreach($Row->getAttributes() as $Atributo => $Valor) $NewArray[$Atributo] = $Row[$Atributo];
      unset($NewArray["_id"]);unset($NewArray["updated_at"]);unset($NewArray["created_at"]);unset($NewArray["updatedAt"]);
      $ModelName::on("general")->where("_id",$Row->_id)->update($NewArray,["upsert"=>true]);
      $Contador++;
    }
    echo $Contador." Registros de <b>".$Message."</b><br>";
  }
}
if (!function_exists('ClaveConfiguracion')) {
  function ClaveConfiguracion(){
    return encode('55gfd5dfg5hgh45joi54ñ56iop4p65i4uy6t5g4f32x1nh3j5tyu4i98g7hg6c54vg6w');
  }
}
/*** FIN BACKUP **/
if (!function_exists('WebhookProduction')) {
  function WebhookProduction(){
    \SSH::into("tabulatest")->run([
      'cd ~/public_html/wwv/',
      'rm .git/index.lock',    
    ], function($line){echo "#".($line.PHP_EOL."<br>");});
    \SSH::into("tabulatest")->run([
      'cd ~/public_html/wwv/',
      'git checkout production',
      'git pull',
      'git log -n 1'
    ], function($line){echo "#".($line.PHP_EOL."<br>");});
  }
}
if (!function_exists('return_ajax')) {
  function return_ajax($array){
    if(session("ERROR_INSITUCION") == null)
      return json_encode($array);
    else{
      return json_encode(["status"=>509,"msg"=>"F5"]);
    }
  }
}
if (!function_exists('WebhookMaster')) {
  function WebhookMaster(){
    \SSH::into("integritic")->run([
      'cd ~/public_html/tabulatest-laravel/',
      'rm .git/index.lock',    
    ], function($line){echo "#".($line.PHP_EOL."<br>");});
    \SSH::into("integritic")->run([
      'cd ~/public_html/tabulatest-laravel/',
      'git checkout master',
      'git pull',
      'git log -n 1'
    ], function($line){echo "#".($line.PHP_EOL."<br>");});
  }
}
if (!function_exists('WebhookMantenedores')) {
  function WebhookMantenedores(){
    \SSH::into("mantenedores")->run([
      'cd /Mantenedor/mantenedores',
      'rm .git/index.lock',    
    ], function($line){echo "#".($line.PHP_EOL."<br>");});
    \SSH::into("mantenedores")->run([
      'cd /Mantenedor/mantenedores/',
      'git checkout master',
      'git pull',
      'git log -n 1'
    ], function($line){echo "#".($line.PHP_EOL."<br>");});
  }
}
if (!function_exists('fill_filter_alumnos_evaluados')) {
  function fill_filter_alumnos_evaluados($ALUMNOS,$SS_ALUMNOS,$POST,$CONTADOR=[]){
    $ALUMNOS = array_filter($ALUMNOS, function($e) use(&$POST,&$CONTADOR,&$SS_ALUMNOS){
      if((int)$e["TEST"]["TEST"]["NUMERO"]!=(int)$POST["ntest"]||(String)$e["TEST"]["TEST"]["CATEGORIA"]!=$POST["categoria"]){
          !isset($CONTADOR[$e["ALUMNO"]])?$CONTADOR[$e["ALUMNO"]]=0:false;$CONTADOR[$e["ALUMNO"]]++;
          if($CONTADOR[$e["ALUMNO"]]==$e["TOTAL_REALIZADOS"]){
              $e["ID_EVALUACION"]=null;$e["ID_TEST"]=null;
              $e["ESTADO"]=getEstadoEvaluacion('1');
              $e["ACCIONES"]=0;$e["TEST"]=['TEST'=>[ 'NUMERO' => (int)$POST["ntest"], 'CATEGORIA'=>$POST["categoria"],'TIPO' =>'NORMAL','ESTADO'=>'0']];
              $SS_ALUMNOS[$e["ALUMNO"]]=$e;
              return $e;
          }
          return false;
      }
      $SS_ALUMNOS[$e["ALUMNO"]]=$e;
      return $e;
  });
    return $SS_ALUMNOS;
  }
}





if (!function_exists('claveMaestra')) {
  function claveMaestra(){
    return encode('Crci1704');
  }
}
if (!function_exists('fillList')) {
  function fillList($id,$text,$array,$orden=""){
    $aux = [];
        for($i = 0; $i < count($array) ; $i++)
        array_push($aux, [
            'id'   => array_column($array, $id)[$i],
            'text' => (array_column($array, $text)[$i]),
            'orden' => array_column($array, $orden)[$i]
        ]);
    return $aux;
  }
}
if (!function_exists('fillList_id')) {
  function fillList_id($id,$text,$array){
    $aux = [];
        for($i = 0; $i < count($array) ; $i++)
        array_push($aux, [
            'id'   => (String)array_column($array, $id)[$i],
            'text' => (array_column($array, $text)[$i])
        ]);
    return $aux;
  }
}
if (!function_exists('unique_multidim_array')) {
  function unique_multidim_array($array, $key) { 
      $temp_array = array(); 
      $i = 0; 
      $key_array = array(); 
      
      foreach($array as $val) { 
          if (!in_array($val[$key], $key_array)) { 
              $key_array[$i] = $val[$key]; 
              $temp_array[$i] = $val; 
          } 
          $i++; 
      } 
      return $temp_array; 
  } 
}
if (!function_exists('getLetrasAlternativas')) {
  function getLetrasAlternativas(){
    return ["a", "b", "c", "d", "e","f", "g", "h", "i", "j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
  }
}
if (!function_exists('getCategoriaPalabra')) {
  function getCategoriaPalabra($id){
    switch ($id) {
      case '1': return "MONOSILABOS";break;
      case '2': return "BISILABAS";break;
      case '3': return "TRISILABAS";break;
      case '4': return "TETRASILABOS";break;
      case '5': return "POLISILABAS";break;
    }    
  }
}
if (!function_exists('getCategoriaLetra')) {
  function getCategoriaLetra($id){
    switch ($id) {
      case '1': return ["a", "e", "i", "o", "u","A", "E", "I", "O", "U"];break;
      case '2': return ["b","c","d","f","g","h","j","k","l","m","n","p","q","r","s","t","v","w","x","y","z"];break;
      case '3': return [
      "A","B","C","D","E","F","G","H","K","M","N","P","R","S","T","U","V","W","X","Y","Z",
      "b","c","d","e","f","h","k","m","n","o","p","r","s","t","u","w","x","z"];break;
      case 'COMPRENSION': return ["a", "b", "c", "d", "e","f", "g", "h", "i", "j"];break;
    }    
  }
}
if (!function_exists('getCategoriaPalabras')) {
  function getCategoriaPalabras($id){
    $palabras = explode("-", $id);
    $_palabras = [];
    foreach ($palabras as $id) array_push($_palabras, getCategoriaPalabra($id));
    $palabras = \App\Models\LTCategoriasPalabras::getPalabras($_palabras);
    $palabras = array_column($palabras, "PALABRAS");
    $_palabras = [];
    foreach($palabras as $array) foreach ($array as $x) array_push($_palabras, $x);
    return $_palabras;
  }
}
if (!function_exists('array_orderby')) {
function array_orderby()
{
    $args = func_get_args();
    $data = array_shift($args);
    foreach ($args as $n => $field) {
        if (is_string($field)) {
            $tmp = array();
            foreach ($data as $key => $row)
                $tmp[$key] = $row[$field];
            $args[$n] = $tmp;
            }
    }
    $args[] = &$data;
    call_user_func_array('array_multisort', $args);
    return array_pop($args);
  }
}
if (!function_exists('format_array')) {
  function format_array($item,$item2) {
      return $item[$item2];
  }
}
if (!function_exists('MongoToArray')) {
  function MongoToArray($mongo,$id=[],$array=[],$obj=[],$date=[],$capitalize=[],$return=[]){
     foreach($mongo as $object){
        $aux=[];
        foreach($object as $key=>$data){
            $is_id=array_search($key, $id)===false?false:true;
            $is_array=array_search($key, $array)===false?false:true;
            $is_date=array_search($key, $date)===false?false:true;
            $is_date=array_search($key, $date)===false?false:true;
            $is_capitalize=array_search($key, $capitalize)===false?false:true;            
            $is_obj=array_search($key, $obj)===false?false:true;
            $store=$data;
            $store=$is_id?(String)($store):$store;
            $store=$is_capitalize?(String)nombrePropio($store):$store;            
            $store=$is_date?getMongoDate($store):$store;
            $store=$is_array?MongoToArray($store,$id,$array,$obj,$date):$store;            
            $store=$is_obj?MongoToArray([0=>$store],$id,$array,$obj,$date)[0]:$store;
            $aux[$key]=$store;
        }
        array_push($return,$aux);
    }
    return $return;
  }
}
if (!function_exists('initFileInput')) {
  function initFileInput(){
    return 
    '<script type="text/javascript">
      var imgTemp = "'.getDefaultImg().'";
      $( document ).ready(function() {    
        $(".file-input").fileinput({
            browseLabel: "Buscar",
            browseClass: "btn btn-primary btn-icon",
            removeLabel: "&nbsp;",      
            showUpload: false,  
            preferIconicPreview: true,    
            allowedFileExtensions: ["jpg", "gif", "png"],
            defaultPreviewContent: "<img src=\'"+imgTemp+"\' id=\'previewImg\' alt=\'Your Avatar\' style=\'width:160px\'>",
            removeClass: "btn btn-danger btn-icon",     
            initialCaption: "Seleccione una imagen",        
            resizeImage: true,
            maxImageWidth: 128,
            maxImageHeight: 128,      
        });     
      }); 
    </script>';
  }
}
if (!function_exists('getAssets')) {
  function getImgVak(){
    return [
          "V"=>asset('assets/images/letrapps/vak_V.png'),
          "A"=>asset('assets/images/letrapps/vak_A.png'),
          "K"=>asset('assets/images/letrapps/vak_K.png'),
          "AV"=>asset('assets/images/letrapps/vak_AV.png'),
          "VA"=>asset('assets/images/letrapps/vak_AV.png'),
          "KA"=>asset('assets/images/letrapps/vak_KA.png'),
          "AK"=>asset('assets/images/letrapps/vak_KA.png'),
          "VK"=>asset('assets/images/letrapps/vak_VK.png'),
          "KV"=>asset('assets/images/letrapps/vak_VK.png'),
        ];
  }

}
if (!function_exists('getAssets')) {
  function getAssets($NOMBRECURSO=""){
      return [
        "LOGOPLAT"=>public_path('assets/images/logo_login.png'),
        "VAK"=>[
          "V"=>public_path('assets/images/letrapps/vak_V.png'),
          "A"=>public_path('assets/images/letrapps/vak_A.png'),
          "K"=>public_path('assets/images/letrapps/vak_K.png'),
          "AV"=>public_path('assets/images/letrapps/vak_AV.png'),
          "VA"=>public_path('assets/images/letrapps/vak_AV.png'),
          "KA"=>public_path('assets/images/letrapps/vak_KA.png'),
          "AK"=>public_path('assets/images/letrapps/vak_KA.png'),
          "VK"=>public_path('assets/images/letrapps/vak_VK.png'),
          "KV"=>public_path('assets/images/letrapps/vak_VK.png'),
        ],
        "IMAGEN"=>[
          "M"=>["BASICO"=>public_path('assets/images/letrapps/M_BASICO.png'),"MEDIO"=>public_path('assets/images/letrapps/M_MEDIO.png')],
          "F"=>["BASICO"=>public_path('assets/images/letrapps/F_BASICO.png'),"MEDIO"=>public_path('assets/images/letrapps/F_MEDIO.png')]
        ],
      
        "COLOR" => ["M"=>"#BDE6FA","F"=>"#FCD3C2"],
        "LOGOINSTITUCION"=>session('INSTITUCION.LOGO'),
        "NOMBREINSTITUCION"=>session('INSTITUCION.NOMBRE'),
      ];
    }
}
if(!function_exists("definicion_vak")){
  function definicion_vak($VAK,$_1="",$_2=""){
    switch ($VAK) {
      case 'V': 
      return "<b style='color:#00b5b8'>Visual</b>: Entiende el mundo tal como lo ve; el aspecto de las cosas es lo más importante.<br>Cuando recuerda algo lo hace en forma de imágenes.";break;
      case 'A': 
      return "<b style='color:#4747b1'>Auditivo</b>: Entiende el mundo a través del lenguaje; aprende lo que oye.";break;
      case 'K': 
      return "<b style='color:#ea6c5c'>Kinestésico</b>: Entiende el mundo a través de de la experimentación directa; comprende mejor cuando pone en práctica las cosas.";break;
      case 'AV': 
      return "<b style='color:#00b5b8'>Visual</b>: Entiende el mundo tal como lo ve; el aspecto de las cosas es lo más importante.<br>Cuando recuerda algo lo hace en forma de imágenes.<br>
      <b style='color:#4747b1'>Auditivo</b>: Entiende el mundo a través del lenguaje; aprende lo que oye.";break;
      case 'VA': 
      return "<b style='color:#00b5b8'>Visual</b>: Entiende el mundo tal como lo ve; el aspecto de las cosas es lo más importante.<br>Cuando recuerda algo lo hace en forma de imágenes.<br>
      <b style='color:#4747b1'>Auditivo</b>: Entiende el mundo a través del lenguaje; aprende lo que oye.";break;
      case 'AK': 
      return "<b style='color:#4747b1'>Auditivo</b>: Entiende el mundo a través del lenguaje; aprende lo que oye.<br>
      <b style='color:#ea6c5c'>Kinestésico</b>: Entiende el mundo a través de de la experimentación directa; comprende mejor cuando pone en práctica las cosas.";break;
      case 'KA': 
      return "<b style='color:#4747b1'>Auditivo</b>: Entiende el mundo a través del lenguaje; aprende lo que oye.<br>
      <b style='color:#ea6c5c'>Kinestésico</b>: Entiende el mundo a través de de la experimentación directa; comprende mejor cuando pone en práctica las cosas.";break;
      case 'KV': 
      return "<b style='color:#00b5b8'>Visual</b>: Entiende el mundo tal como lo ve; el aspecto de las cosas es lo más importante.<br>Cuando recuerda algo lo hace en forma de imágenes.<br>
      <b style='color:#ea6c5c'>Kinestésico</b>: Entiende el mundo a través de de la experimentación directa; comprende mejor cuando pone en práctica las cosas.";break;
      case 'VK' : 
      return "<b style='color:#00b5b8'>Visual</b>: Entiende el mundo tal como lo ve; el aspecto de las cosas es lo más importante.<br>Cuando recuerda algo lo hace en forma de imágenes.<br>
      <b style='color:#ea6c5c'>Kinestésico</b>: Entiende el mundo a través de de la experimentación directa; comprende mejor cuando pone en práctica las cosas.";break;
    }
  }
}
if(!function_exists("getColorVak")){
  function getColorVak($VAK){
    switch ($VAK) {
      case 'V': return '#00b5b8';break;
      case 'A': return '#4747b1';break;
      case 'K': return '#ea6c5c';break;
      case 'AV': return '#237eb4';break;
      case 'VA': return '#237eb4';break;
      case 'VK': return '#75908a';break;
      case 'KV': return '#75908a';break;
      case 'AK': return '#985986';break;
      case 'KA': return '#985986';break;
    }
  }
}
if(!function_exists("getResultadoVak")){
  function getResultadoVak($VAK){
    switch ($VAK) {
      case 'V': return 'V';break;
      case 'A': return 'A';break;
      case 'K': return 'K';break;
      case 'AV': return 'VA';break;
      case 'VA': return 'VA';break;
      case 'VK': return 'VK';break;
      case 'KV': return 'VK';break;
      case 'AK': return 'AK';break;
      case 'KA': return 'AK';break;
    }
  }
}
if(!function_exists("getNombreVak")){
  function getNombreVak($VAK){
    switch ($VAK) {
      case 'V': return 'Visual';break;
      case 'A': return 'Auditivo';break;
      case 'K': return 'Kinestésico';break;
      case 'AV': return 'Visual • Auditivo';break;
      case 'VA': return 'Visual • Auditivo';break;
      case 'VK': return 'Visual • Kinestésico';break;
      case 'KV': return 'Visual • Kinestésico';break;
      case 'AK': return 'Auditivo • Kinestésico';break;
      case 'KA': return 'Auditivo • Kinestésico';break;
    }
  }
}



if(!function_exists("resultado_vak")){
  function resultado_vak($VAK,$MAX,$_1="",$_2=""){
    $porcentaje=round(((int)$_1/(int)$MAX)*100,1);
    $html='<div style="width: 100%; text-align: center;font-size: 11px;">';
    switch ($VAK) {
      case 'V': 
      return  '<div style="width: 100%; text-align: center;font-size: 11px;color:#00b5b8;font-weight:bold;">'."Estilo Visual • $_1 respuestas con un $porcentaje% del total"."</div>";break;
      
      case 'A': 
      return  '<div style="width: 100%; text-align: center;font-size: 11px;color:#4747b1;font-weight:bold;">'."Estilo Auditivo • $_1 respuestas con un $porcentaje% del total"."</div>";break;
      
      case 'K': 
      return  '<div style="width: 100%; text-align: center;font-size: 11px;color:#ea6c5c;font-weight:bold;">'."Estilo Kinestésico • $_1 respuestas con un $porcentaje% del total"."</div>";break;
      
      case 'AV': 
      return  '<div style="width: 100%; text-align: center;font-size: 11px;color:#4747b1;font-weight:bold;">'."Estilo Auditivo • $_1 respuestas con un $porcentaje% del total"."</div>".
      '<div style="width: 100%; text-align: center;font-size: 11px;color:#00b5b8;font-weight:bold;">'."Estilo Visual • $_2 respuestas con un $porcentaje% del total"."</div>";break;
      case 'VA': 
      return  '<div style="width: 100%; text-align: center;font-size: 11px;color:#4747b1;font-weight:bold;">'."Estilo Auditivo • $_2 respuestas con un $porcentaje% del total"."</div>".
      '<div style="width: 100%; text-align: center;font-size: 11px;color:#00b5b8;font-weight:bold;">'."Estilo Visual • $_1 respuestas con un $porcentaje% del total"."</div>";break;
      
      case 'KA': 
      return  '<div style="width: 100%; text-align: center;font-size: 11px;color:#4747b1;font-weight:bold;">'."Estilo Auditivo • $_2 respuestas con un $porcentaje% del total"."</div>".'<div style="width: 100%; text-align: center;font-size: 11px;color:#ea6c5c;font-weight:bold;">'."Estilo Kinestésico • $_1 respuestas con un $porcentaje% del total"."</div>";break;
      case 'AK': 
      return  '<div style="width: 100%; text-align: center;font-size: 11px;color:#4747b1;font-weight:bold;">'."Estilo Auditivo • $_2 respuestas con un $porcentaje% del total"."</div>".'<div style="width: 100%; text-align: center;font-size: 11px;color:#ea6c5c;font-weight:bold;">'."Estilo Kinestésico • $_1 respuestas con un $porcentaje% del total"."</div>";break;

      case 'VK' : 
      return  '<div style="width: 100%; text-align: center;font-size: 11px;color:#00b5b8;font-weight:bold;">'."Estilo Visual • $_1 respuestas con un $porcentaje% del total"."</div>".
      '<div style="width: 100%; text-align: center;font-size: 11px;color:#ea6c5c;font-weight:bold;">'."Estilo Kinestésico • $_2 respuestas con un $porcentaje% del total"."</div>";break;
      case 'KV': 
      return  '<div style="width: 100%; text-align: center;font-size: 11px;color:#00b5b8;font-weight:bold;">'."Estilo Visual • $_2 respuestas con un $porcentaje% del total"."</div>".
      '<div style="width: 100%; text-align: center;font-size: 11px;color:#ea6c5c;font-weight:bold;">'."Estilo Kinestésico • $_1 respuestas con un $porcentaje% del total"."</div>";break;
    }
  }
}
if(!function_exists("sugerencias_curso_vak")){
  function sugerencias_curso_vak($VAK){
    switch ($VAK) {
      case 'V': 
      return "El curso obtuvo como resultado el estilo de aprendizaje <b>VISUAL</b>, lo que significa que entienden el mundo tal como lo ven, el aspecto de las cosas es lo más importante y cuando recuerdan algo lo hacen en forma de imágenes.<br>Se sugiere que en el hogar escriba una lista con los quehaceres escolares a realizar diariamente, dividiendo la información paso a paso. - Leer utilizando el subrayado de ideas principales (usar colores para favorecer la extracción de información). - Resumir ideas principales por medio de esquemas o tablas que favorezcan la organización de la información.";break;
      case 'A': 
      return "El curso obtuvo como resultado el estilo de aprendizaje <b>AUDITIVO</b>, lo que significa que entienden el mundo a través del lenguaje y aprende lo que oye.<br>Se sugiere que en el hogar se puedan repasar los contenidos mediante repeticiones rítmicas en voz alta y posteriormente verbalizar lo aprendido a través de preguntas orales y resumir los contenidos educativos mediante fichas y cuestionarios.";break;
      case 'K': 
      return "El curso obtuvo como resultado el estilo de aprendizaje <b>KINESTÉSICO</b>, lo que significa que entienden el mundo a través de  la experimentación directa, comprendiendo mejor cuando pone en práctica las cosas.<br>Se sugiere utilizar gestos para acompañar las instrucciones entregadas en el hogar , también se sugiere asociar los contenidos con movimientos o sensaciones corporales: Ejemplo, mientras estudia puede mover una pelota, un lápiz una goma, caminar (corporal); comer algo que le guste, masticar chicle, etc (sensaciones).";break;

      case 'AV': 
      return "El curso obtuvo una combinación de los estilo de aprendizaje <b>VISUAL</b> y <b>AUDITIVO</b>, lo que significa que entienden el mundo tal como lo ven, cuando recuerdan algo lo hacen en forma de imágenes, a través del lenguaje y también aprendiendo de forma auditiva.<br>Se sugiere en el hogar escribir una lista con los quehaceres escolares a realizar diariamente, dividiendo la información paso a paso. - Leer utilizando el subrayado de ideas principales (usar colores para favorecer la extracción de información). - Resumir ideas principales por medio de esquemas o tablas que favorezcan la organización de la información.<br>Repasar los contenidos mediante repeticiones rítmicas en voz alta y posteriormente verbalizar lo aprendido a través de preguntas orales y resumir los contenidos educativos mediante fichas y cuestionarios.";break;
      case 'VA': 
      return "El curso obtuvo una combinación de los estilo de aprendizaje <b>VISUAL</b> y <b>AUDITIVO</b>, lo que significa que entienden el mundo tal como lo ven, cuando recuerdan algo lo hacen en forma de imágenes, a través del lenguaje y también aprendiendo de forma auditiva.<br>Se sugiere en el hogar escribir una lista con los quehaceres escolares a realizar diariamente, dividiendo la información paso a paso. - Leer utilizando el subrayado de ideas principales (usar colores para favorecer la extracción de información). - Resumir ideas principales por medio de esquemas o tablas que favorezcan la organización de la información.<br>Repasar los contenidos mediante repeticiones rítmicas en voz alta y posteriormente verbalizar lo aprendido a través de preguntas orales y resumir los contenidos educativos mediante fichas y cuestionarios.";break;
      
      case 'AK': 
      return "El curso obtuvo una combinación de los estilo de aprendizaje <b>AUDITIVO</b> y <b>KINESTÉSICO</b>, lo que significa que entienden el mundo tanto a través del lenguaje, aprendiendo de forma auditiva, como también por medio de la experimentación directa, comprendiendo mejor cuando pone en práctica las cosas.<br>Se sugiere que en el hogar se puedan repasar los contenidos mediante repeticiones rítmicas en voz alta y posteriormente verbalizar lo aprendido a través de preguntas orales y resumir los contenidos educativos mediante fichas y cuestionarios.<br>Utilizar gestos para acompañar las instrucciones entregadas en el hogar , también se sugiere asociar los contenidos con movimientos o sensaciones corporales: Ejemplo, mientras estudia puede mover una pelota, un lápiz una goma, caminar (corporal); comer algo que le guste, masticar chicle, etc (sensaciones).";break;
      case 'KA': 
       return "El curso obtuvo una combinación de los estilo de aprendizaje <b>AUDITIVO</b> y <b>KINESTÉSICO</b>, lo que significa que entienden el mundo tanto a través del lenguaje, aprendiendo de forma auditiva, como también por medio de la experimentación directa, comprendiendo mejor cuando pone en práctica las cosas.<br>Se sugiere que en el hogar se puedan repasar los contenidos mediante repeticiones rítmicas en voz alta y posteriormente verbalizar lo aprendido a través de preguntas orales y resumir los contenidos educativos mediante fichas y cuestionarios.<br>Utilizar gestos para acompañar las instrucciones entregadas en el hogar , también se sugiere asociar los contenidos con movimientos o sensaciones corporales: Ejemplo, mientras estudia puede mover una pelota, un lápiz una goma, caminar (corporal); comer algo que le guste, masticar chicle, etc (sensaciones).";break;
      
      case 'VK': 
      return "El curso obtuvo una combinación de los estilo de aprendizaje <b>VISUAL</b> y <b>KINESTÉSICO</b>, lo que significa que entienden el mundo tal como lo ven, cuando recuerdan algo lo hacen en forma de imágenes, como también por medio de la experimentación directa, comprendiendo mejor cuando pone en práctica las cosas.<br>Se sugiere en el hogar escribir una lista con los quehaceres escolares a realizar diariamente, dividiendo la información paso a paso. - Leer utilizando el subrayado de ideas principales (usar colores para favorecer la extracción de información). - Resumir ideas principales por medio de esquemas o tablas que favorezcan la organización de la información.<br>Utilizar gestos para acompañar las instrucciones entregadas en el hogar , también se sugiere asociar los contenidos con movimientos o sensaciones corporales: Ejemplo, mientras estudia puede mover una pelota, un lápiz una goma, caminar (corporal); comer algo que le guste, masticar chicle, etc (sensaciones).";break;
      case 'KV': 
      return "El curso obtuvo una combinación de los estilo de aprendizaje <b>VISUAL</b> y <b>KINESTÉSICO</b>, lo que significa que entienden el mundo tal como lo ve, cuando recuerdan algo lo hacen en forma de imágenes, como también por medio de la experimentación directa, comprendiendo mejor cuando pone en práctica las cosas.<br>Se sugiere en el hogar escribir una lista con los quehaceres escolares a realizar diariamente, dividiendo la información paso a paso. - Leer utilizando el subrayado de ideas principales (usar colores para favorecer la extracción de información). - Resumir ideas principales por medio de esquemas o tablas que favorezcan la organización de la información.<br>Utilizar gestos para acompañar las instrucciones entregadas en el hogar , también se sugiere asociar los contenidos con movimientos o sensaciones corporales: Ejemplo, mientras estudia puede mover una pelota, un lápiz una goma, caminar (corporal); comer algo que le guste, masticar chicle, etc (sensaciones).";break;
    }
  }
}
if(!function_exists("sugerencias_vak")){
  function sugerencias_vak($VAK){
    switch ($VAK) {
      case 'V': 
      return "<b>XXXXXXXXXX</b> obtuvo como resultado el estilo de aprendizaje <b>VISUAL</b>, lo que significa que XX entiende el mundo tal como lo ve, el aspecto de las cosas es lo más importante y cuando recuerda algo lo hace en forma de imágenes.<br>Se sugiere que en el hogar escriba una lista con los quehaceres escolares a realizar diariamente, dividiendo la información paso a paso. - Leer utilizando el subrayado de ideas principales (usar colores para favorecer la extracción de información). - Resumir ideas principales por medio de esquemas o tablas que favorezcan la organización de la información.";break;
      case 'A': 
      return "<b>XXXXXXXXXX</b> obtuvo como resultado el estilo de aprendizaje <b>AUDITIVO</b>, lo que significa que XX entiende el mundo a través del lenguaje y aprende lo que oye.<br>Se sugiere que en el hogar se puedan repasar los contenidos mediante repeticiones rítmicas en voz alta y posteriormente verbalizar lo aprendido a través de preguntas orales y resumir los contenidos educativos mediante fichas y cuestionarios.";break;
      case 'K': 
      return "<b>XXXXXXXXXX</b> obtuvo como resultado el estilo de aprendizaje <b>KINESTÉSICO</b>, lo que significa que XX entiende el mundo a través de  la experimentación directa, comprendiendo mejor cuando pone en práctica las cosas.<br>Se sugiere utilizar gestos para acompañar las instrucciones entregadas en el hogar , también se sugiere asociar los contenidos con movimientos o sensaciones corporales: Ejemplo, mientras estudia puede mover una pelota, un lápiz una goma, caminar (corporal); comer algo que le guste, masticar chicle, etc (sensaciones).";break;

      case 'AV': 
      return "<b>XXXXXXXXXX</b> obtuvo una combinación de los estilo de aprendizaje <b>VISUAL</b> y <b>AUDITIVO</b>, lo que significa que XX entiende el mundo tal como lo ve, cuando recuerda algo lo hace en forma de imágenes, a través del lenguaje y también aprendiendo de forma auditiva.<br>Se sugiere en el hogar escribir una lista con los quehaceres escolares a realizar diariamente, dividiendo la información paso a paso. - Leer utilizando el subrayado de ideas principales (usar colores para favorecer la extracción de información). - Resumir ideas principales por medio de esquemas o tablas que favorezcan la organización de la información.<br>Repasar los contenidos mediante repeticiones rítmicas en voz alta y posteriormente verbalizar lo aprendido a través de preguntas orales y resumir los contenidos educativos mediante fichas y cuestionarios.";break;
      case 'VA': 
      return "<b>XXXXXXXXXX</b> obtuvo una combinación de los estilo de aprendizaje <b>VISUAL</b> y <b>AUDITIVO</b>, lo que significa que XX entiende el mundo tal como lo ve, cuando recuerda algo lo hace en forma de imágenes, a través del lenguaje y también aprendiendo de forma auditiva.<br>Se sugiere en el hogar escribir una lista con los quehaceres escolares a realizar diariamente, dividiendo la información paso a paso. - Leer utilizando el subrayado de ideas principales (usar colores para favorecer la extracción de información). - Resumir ideas principales por medio de esquemas o tablas que favorezcan la organización de la información.<br>Repasar los contenidos mediante repeticiones rítmicas en voz alta y posteriormente verbalizar lo aprendido a través de preguntas orales y resumir los contenidos educativos mediante fichas y cuestionarios.";break;
      
      case 'AK': 
      return "<b>XXXXXXXXXX</b> obtuvo una combinación de los estilo de aprendizaje <b>AUDITIVO</b> y <b>KINESTÉSICO</b>, lo que significa que XX entiende el mundo tanto a través del lenguaje, aprendiendo de forma auditiva, como también por medio de la experimentación directa, comprendiendo mejor cuando pone en práctica las cosas.<br>Se sugiere que en el hogar se puedan repasar los contenidos mediante repeticiones rítmicas en voz alta y posteriormente verbalizar lo aprendido a través de preguntas orales y resumir los contenidos educativos mediante fichas y cuestionarios.<br>Utilizar gestos para acompañar las instrucciones entregadas en el hogar , también se sugiere asociar los contenidos con movimientos o sensaciones corporales: Ejemplo, mientras estudia puede mover una pelota, un lápiz una goma, caminar (corporal); comer algo que le guste, masticar chicle, etc (sensaciones).";break;
      case 'KA': 
       return "<b>XXXXXXXXXX</b> obtuvo una combinación de los estilo de aprendizaje <b>AUDITIVO</b> y <b>KINESTÉSICO</b>, lo que significa que XX entiende el mundo tanto a través del lenguaje, aprendiendo de forma auditiva, como también por medio de la experimentación directa, comprendiendo mejor cuando pone en práctica las cosas.<br>Se sugiere que en el hogar se puedan repasar los contenidos mediante repeticiones rítmicas en voz alta y posteriormente verbalizar lo aprendido a través de preguntas orales y resumir los contenidos educativos mediante fichas y cuestionarios.<br>Utilizar gestos para acompañar las instrucciones entregadas en el hogar , también se sugiere asociar los contenidos con movimientos o sensaciones corporales: Ejemplo, mientras estudia puede mover una pelota, un lápiz una goma, caminar (corporal); comer algo que le guste, masticar chicle, etc (sensaciones).";break;
      
      case 'VK': 
      return "<b>XXXXXXXXXX</b> obtuvo una combinación de los estilo de aprendizaje <b>VISUAL</b> y <b>KINESTÉSICO</b>, lo que significa que XX entiende el mundo tal como lo ve, cuando recuerda algo lo hace en forma de imágenes, como también por medio de la experimentación directa, comprendiendo mejor cuando pone en práctica las cosas.<br>Se sugiere en el hogar escribir una lista con los quehaceres escolares a realizar diariamente, dividiendo la información paso a paso. - Leer utilizando el subrayado de ideas principales (usar colores para favorecer la extracción de información). - Resumir ideas principales por medio de esquemas o tablas que favorezcan la organización de la información.<br>Utilizar gestos para acompañar las instrucciones entregadas en el hogar , también se sugiere asociar los contenidos con movimientos o sensaciones corporales: Ejemplo, mientras estudia puede mover una pelota, un lápiz una goma, caminar (corporal); comer algo que le guste, masticar chicle, etc (sensaciones).";break;
      case 'KV': 
      return "<b>XXXXXXXXXX</b> obtuvo una combinación de los estilo de aprendizaje <b>VISUAL</b> y <b>KINESTÉSICO</b>, lo que significa que XX entiende el mundo tal como lo ve, cuando recuerda algo lo hace en forma de imágenes, como también por medio de la experimentación directa, comprendiendo mejor cuando pone en práctica las cosas.<br>Se sugiere en el hogar escribir una lista con los quehaceres escolares a realizar diariamente, dividiendo la información paso a paso. - Leer utilizando el subrayado de ideas principales (usar colores para favorecer la extracción de información). - Resumir ideas principales por medio de esquemas o tablas que favorezcan la organización de la información.<br>Utilizar gestos para acompañar las instrucciones entregadas en el hogar , también se sugiere asociar los contenidos con movimientos o sensaciones corporales: Ejemplo, mientras estudia puede mover una pelota, un lápiz una goma, caminar (corporal); comer algo que le guste, masticar chicle, etc (sensaciones).";break;
    }
  }
}
if (!function_exists('getAssetsCurso')) {
  function getAssetsCurso(){
      return [
        "LOGOPLAT"=>public_path('assets/images/logo_login.png'),
        "LOGOINSTITUCION"=>session('INSTITUCION.LOGO'),
        "NOMBREINSTITUCION"=>session('INSTITUCION.NOMBRE'),
        "imagen_info_curso"=>(public_path('assets/images/letrapps/libro_cerebro.png')),
      ];
    }
}
if (!function_exists('getAssetsDiploma')) {
  function getAssetsDiploma(){
      return [
        "LOGOPLAT"=>public_path('assets/images/logo_login.png'),
        "LOGOINSTITUCION"=>session('INSTITUCION.LOGO'),
        "NOMBREINSTITUCION"=>session('INSTITUCION.NOMBRE'),
        "BORDE_DIPLOMA"=>(public_path('assets/images/letrapps/marco_diploma.png')),
      ];
    }
}
if (!function_exists('getCategoriaCalidad2')) {
  function getCategoriaCalidad2($nivel){
    $aux = quitarAcentos(str_replace("Lectura ", "", $nivel));
    $aux = str_replace("SILABICA", "SILÁBICA", $aux);
    return mb_strtoupper($aux);
  }
}
if (! function_exists('getNombreCurso')) {
    function getNombreCurso($str) { 
        switch ($str) {
            case "PRE KINDER": return ["Pre Kinder",0,"BASICO"]; break;
            case "KINDER": return ["Kinder",1,"BASICO"]; break;
            case "1 BASICO": return ["1° Básico",2,"BASICO"]; break;
            case "2 BASICO": return ["2° Básico",3,"BASICO"]; break;
            case "3 BASICO": return ["3° Básico",4,"BASICO"]; break;
            case "4 BASICO": return ["4° Básico",5,"BASICO"]; break;
            case "5 BASICO": return ["5° Básico",6,"BASICO"]; break;
            case "6 BASICO": return ["6° Básico",7,"BASICO"]; break;
            case "7 BASICO": return ["7° Básico",8,"BASICO"]; break;
            case "8 BASICO": return ["8° Básico",9,"BASICO"]; break;
            case "1 MEDIO": return ["1° Medio",10,"MEDIO"]; break;
            case "2 MEDIO": return ["2° Medio",11,"MEDIO"]; break;
            case "3 MEDIO": return ["3° Medio",12,"MEDIO"]; break;
            case "4 MEDIO": return ["4° Medio",13,"MEDIO"]; break;
        }        
    }
}
if (! function_exists('getOrdenCurso')) {
    function getOrdenCurso($str) { 
        switch ($str) {
            case "PRE KINDER": return 0; break;
            case "KINDER": return 1; break;
            case "1 BASICO": return 2; break;
            case "2 BASICO": return 3; break;
            case "3 BASICO": return 4; break;
            case "4 BASICO": return 5; break;
            case "5 BASICO": return 6; break;
            case "6 BASICO": return 7; break;
            case "7 BASICO": return 8; break;
            case "8 BASICO": return 9; break;
            case "1 MEDIO": return 10; break;
            case "2 MEDIO": return 11; break;
            case "3 MEDIO": return 12; break;
            case "4 MEDIO": return 13; break;
        }        
    }
}
if (! function_exists('getMonoCurso')) {
    function getMonoCurso($curso) {       
      switch ($curso) {
        case 'KINDER': return 'BASICO';break;
        case 'PRE KINDER': return 'BASICO';break;
        case '1 BASICO': return 'BASICO';break;
        case '2 BASICO': return 'BASICO';break;
        case '3 BASICO': return 'BASICO';break;
        case '4 BASICO': return 'BASICO';break;
        case '5 BASICO': return 'MEDIO';break;
        case '6 BASICO': return 'MEDIO';break;
        case '7 BASICO': return 'MEDIO';break;
        case '8 BASICO': return 'MEDIO';break;
        case '1 MEDIO': return 'MEDIO';break;
        case '2 MEDIO': return 'MEDIO';break;
        case '3 MEDIO': return 'MEDIO';break;
        case '4 MEDIO': return 'MEDIO';break;
        default : return 'MEDIO';break;
      } 
    }
}
if (!function_exists('getEstado')) {
  function getEstado($st){
    switch ($st) {
      case '1': $bt = "success";$str = "ACTIVO";break;
      case '0': $bt = "warning";$str = "INACTIVO";break;
      default: $bt = "default";$str = "NO ESPECIFICADO";break;
    }
    return '<span class="label label-'.$bt.'">'.$str.'</span>';
  }
}
if (!function_exists('getEstadoEvaluacion')) {
  function getEstadoEvaluacion($st){
    switch ($st) {
      case '4': $bt = "label-warning";$str = "Retirado";break;
      case '1': $bt = "label-default";$str = "Sin Evaluar";break;
      case '7': $bt = "bg-orange-300";$str = "Evaluación Grupal Asignada";break;
      case '8': $bt = "bg-danger-300";$str = "Evaluación Grupal Bloqueada";break;
      case '2': $bt = "label-primary";$str = "Test en Curso";break;
      case '3': $bt = "label-success";$str = "Evaluado";break;
      case '5': $bt = "label-danger";$str = "Limite de Evaluaciones";break;
      default: $bt = "label-default";$str = "No especificado";break;
    }
    return '<span class="label '.$bt.'">'.$str.'</span>';
  }
}
if (!function_exists('getEstadoEvaluacionGrupalAlumno')) {
  function getEstadoEvaluacionGrupalAlumno($st){
    switch ($st) {
      case '0': $bt = "label-default";$str = "Sin Test Asignado";break;
      case '3': $bt = "bg-orange-300";$str = "Test listo para Rendir";break;
      case '4': $bt = "bg-danger-300";$str = "Test Grupal Bloqueado por el Docente";break;
      case '2': $bt = "label-primary";$str = "Test en Curso";break;
      case '1': $bt = "label-success";$str = "Test Realizado";break;
      default: $bt = "label-default";$str = "No especificado";break;
    }
    return '<span class="label '.$bt.'">'.$str.'</span>';
  }
}
if (!function_exists('getEstadoEvaluacionGrupal')) {
  function getEstadoEvaluacionGrupal($st){
    switch ($st) {
      case '0': $bt = "label-default";$str = "Sin Test Asignado";break;
      case '3': $bt = "bg-orange-300";$str = "Test Grupal Asignado";break;
      case '4': $bt = "bg-danger-300";$str = "Test Grupal Bloqueado";break;
      case '2': $bt = "label-primary";$str = "Test en Curso";break;
      case '1': $bt = "label-success";$str = "Tests Realizados";break;
      default: $bt = "label-default";$str = "No especificado";break;
    }
    return '<span class="label '.$bt.'">'.$str.'</span>';
  }
}

if (!function_exists('makeDirectory')) {
  function makeDirectory($path, $mode = 0777, $recursive = false, $force = false){
     if ($force)
    {
        return @mkdir($path, $mode, $recursive);
    }
    else
    {
        return mkdir($path, $mode, $recursive);
    }
  }
}

if (!function_exists('generateButtonAction')) {
  function generateButtonAction($data,$tooltip,$icon,$class){
    return "<button style='padding: 0 9px;width: 40px;height: 40px;' class='btn border-$class bg-$class btn-icon btn-rounded btn-informe' data-popup='tooltip' title='$tooltip' onclick=$data;><span style='font-weight: bold;' class='$icon'></span></button>";
  }
}


if (!function_exists('getEstadoAlumno')) {
  function getEstadoAlumno($st){
    switch ($st) {
      case '0': $bt = "";$str = "";break;
      case '1': $bt = "";$str = "";break;
      case '3': $bt = "danger";$str = "Re-Matriculado";break;
      case '2': $bt = "warning";$str = "Retirado";break;
      case '4': $bt = "";$str = "";break;
      default: $bt = "default";$str = "NO ESPECIFICADO";break;
    }
    return '<span class="label label-'.$bt.'">'.$str.'</span>';
  }
}
if (!function_exists('compararaArrays')) {
  function compararaArrays($array1, $array2) 
  { 
    foreach($array1 as $key => $value) { 
        if(is_array($value)) { 
              if(!isset($array2[$key])) { 
                  $difference[$key] = $value; 
              } 
              elseif(!is_array($array2[$key])) { 
                  $difference[$key] = $value; 
              } 
              else { 
                  $new_diff = compararaArrays($value, $array2[$key]); 
                  if($new_diff != FALSE) { 
                        $difference[$key] = $new_diff; 
                  } 
              } 
          } 
          elseif(!array_key_exists($key, $array2) || $array2[$key] != $value) { 
              $difference[$key] = $value; 
          } 
    } 
    return !isset($difference) ? 0 : 1; 
  }
}
if (! function_exists('newMongoId')) {
    /**
     * Retorna el Objeto de MongoId
     *
     * @param  mixed  $string
     * @return mixed $object_get
     */
    function newMongoId($str)
    {
        try{
          $aux=new \MongoDB\BSON\ObjectID($str);
          return $aux;
        }
        catch(InvalidArgumentException $err){
          return "";
        }
        
    }
}
if (! function_exists('newMongoDate')) {
    /**
     * Retorna el Objeto de MongoDATE
     *
     * @param  mixed  $string
     * @return mixed $object_get
     */
    function newMongoDate($str)
    {
        return new \MongoDB\BSON\UTCDateTime(strtotime($str)* 1000);
    }
}
if (! function_exists('newMongoDateTime')) {
    /**
     * Retorna el Objeto de MongoDATE
     *
     * @param  mixed  $string
     * @return mixed $object_get
     */
    function newMongoDateTimeMoreTime()
    {
        $TIME = date('Y-m-d H:i:s');
        $TIME = strtotime($TIME . "-3hours -1 minutes");
        return new \MongoDB\BSON\UTCDateTime((int)$TIME*1000);
    }
}
if (! function_exists('newMongoDateTime')) {
    /**
     * Retorna el Objeto de MongoDATE
     *
     * @param  mixed  $string
     * @return mixed $object_get
     */
    function newMongoDateTime()
    {
        $TIME = date('Y-m-d H:i:s');
        $TIME = strtotime($TIME . "-3hours");
        return new \MongoDB\BSON\UTCDateTime((int)$TIME*1000);
    }
}
if (! function_exists('newMongoDateTime4')) {
    /**
     * Retorna el Objeto de MongoDATE
     *
     * @param  mixed  $string
     * @return mixed $object_get
     */
    function newMongoDateTime4()
    {
        $TIME = gmdate('Y-m-d H:i:s');
        $TIME = strtotime($TIME . "-3hours");
        return new \MongoDB\BSON\UTCDateTime((int)$TIME*1000);
    }
}
if (! function_exists('newMongoDateTime2')) {
    /**
     * Retorna el Objeto de MongoDATE
     *
     * @param  mixed  $string
     * @return mixed $object_get
     */
    function newMongoDateTime2($time)
    {
        $TIME = date('Y-m-d H:i:s',strtotime($time));
        $TIME = strtotime($TIME . "-3hours");
        return new \MongoDB\BSON\UTCDateTime((int)$TIME*1000);
    }
}
if ( ! function_exists('getRealIP'))
{
  function getRealIP()
  {

      if (isset($_SERVER["HTTP_CLIENT_IP"]))
      {
          return $_SERVER["HTTP_CLIENT_IP"];
      }
      elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
      {
          return $_SERVER["HTTP_X_FORWARDED_FOR"];
      }
      elseif (isset($_SERVER["HTTP_X_FORWARDED"]))
      {
          return $_SERVER["HTTP_X_FORWARDED"];
      }
      elseif (isset($_SERVER["HTTP_FORWARDED_FOR"]))
      {
          return $_SERVER["HTTP_FORWARDED_FOR"];
      }
      elseif (isset($_SERVER["HTTP_FORWARDED"]))
      {
          return $_SERVER["HTTP_FORWARDED"];
      }
      else
      {
          return $_SERVER["REMOTE_ADDR"];
      }

  }
}
if (! function_exists('encode')) {
    /**
     * Retorna el Objeto de encrypt
     *
     * @param  mixed  $string
     * @return mixed $object_get
     */
    function encode($str)
    {
        return hash('sha256',sha1(md5($str."6qtd9fm3@os")."integritic"));
    }
}
if (! function_exists('getMongoDate')) {
    function getMongoDate($fecha){
        return $fecha->toDateTime()->format('Y-m-d');
    }
}
if (! function_exists('getMongoDateHora')) {
    function getMongoDateHora($fecha){
        return $fecha->toDateTime()->format('d-m-Y H:i:s');
    }
}
if (!function_exists('getMes')) {
  function getMes($st){
    switch ((int)$st) {
      case 1: return "Enero"; break;
      case 2: return "Febrero"; break;
      case 3: return "Marzo"; break;
      case 4: return "Abril"; break;
      case 5: return "Mayo"; break;
      case 6: return "Junio"; break;
      case 7: return "Julio"; break;
      case 8: return "Agosto"; break;
      case 9: return "Septiembre"; break;
      case 10: return "Octubre"; break;
      case 11: return "Noviembre"; break;
      case 12: return "Diciembre"; break;
    }
    return '<span class="label label-'.$bt.'">'.$str.'</span>';
  }
}
if (! function_exists('getMongoDateCompleta')) {
    function getMongoDateCompleta($fecha) { 
        return $fecha->toDateTime()->format('d')." de ".getMes($fecha->toDateTime()->format('m'))." de ".$fecha->toDateTime()->format('Y') ;
    }
}
if (! function_exists('comparaFechaRango')) {
    function comparaFechaRango($inicio, $fin, $fecha) { 
        $inicio_ = strtotime($inicio); 
        $fin_ = strtotime($fin); 
        $fecha_ = strtotime($fecha); 
        return (($fecha_ >= $inicio_) && ($fecha_ <= $fin_));
    }
}
if (! function_exists('rutCompleto')) {
  function rutCompleto($rut){
    try{
      $rut = str_replace('-', '', str_replace('.', '', $rut)) ;
      return number_format(substr($rut,0,-1),0,"",".").'-'.substr($rut,strlen($rut)-1,1);  
    }
    catch(Exception $sdsfs){
      return "";
    } 
  }
}
if (! function_exists('quitarAcentos')) {
    function quitarAcentos ($cadena){
        $originales = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
        $modificadas = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
        $cadena = utf8_decode($cadena);
        $cadena = strtr($cadena, utf8_decode($originales), $modificadas);
        $cadena = mb_strtolower($cadena);
        return mb_strtoupper(utf8_encode($cadena));
    }
}
if (! function_exists('nombrePropio')) {
    function nombrePropio ($cadena){        
        if(strlen($cadena) == 1) return strtolower($cadena);
          else {
            $aux = mb_convert_encoding(mb_convert_case($cadena, MB_CASE_TITLE), "UTF-8");
            $aux = str_replace(" A "," a ",$aux);
            $aux = str_replace(" E "," e ",$aux);
            $aux = str_replace(" O "," o ",$aux);
            $aux = str_replace(" U "," u ",$aux);   
            $aux = str_replace(" Y "," y ",$aux);   
            $aux = str_replace("<Br>A<Br>","<br>a<br>",$aux);
            $aux = str_replace("<Br>E<Br>","<br>e<br>",$aux);
            $aux = str_replace("<Br>O<Br>","<br>o<br>",$aux);
            $aux = str_replace("<Br>U<Br>","<br>u<br>",$aux);
            $aux = str_replace("<Br>Y<Br>","<br>y<br>",$aux);
            
            $aux = str_replace("A-410","A- 410",$aux);
            $aux = str_replace("A-510","A- 510",$aux);
            $aux = str_replace("A-610","A- 610",$aux);

            $aux = str_replace("B-410","B- 410",$aux);
            $aux = str_replace("B-510","B- 510",$aux);
            $aux = str_replace("B-610","B- 610",$aux);

            $aux = str_replace("C-410","C- 410",$aux);
            $aux = str_replace("C-510","C- 510",$aux);
            $aux = str_replace("C-610","C- 610",$aux);

            $aux = str_replace("D-410","D- 410",$aux);
            $aux = str_replace("D-510","D- 510",$aux);
            $aux = str_replace("D-610","D- 610",$aux);

            $aux = str_replace("E-410","E- 410",$aux);
            $aux = str_replace("E-510","E- 510",$aux);
            $aux = str_replace("E-610","E- 610",$aux);

            $aux = str_replace("F-410","F- 410",$aux);
            $aux = str_replace("F-510","F- 510",$aux);
            $aux = str_replace("F-610","F- 610",$aux);

            
            return $aux;
          }
    }
}
if (! function_exists('espaciarCodigo')) {
    function espaciarCodigo($aux){   

      $aux = str_replace("A-410","A-<span style='margin-left:1.5px;'>410</span>",$aux);
      $aux = str_replace("A-510","A-<span style='margin-left:1.5px;'>510</span>",$aux);
      $aux = str_replace("A-610","A-<span style='margin-left:1.5px;'>610</span>",$aux);

      $aux = str_replace("B-410","B-<span style='margin-left:1.5px;'>410</span>",$aux);
      $aux = str_replace("B-510","B-<span style='margin-left:1.5px;'>510</span>",$aux);
      $aux = str_replace("B-610","B-<span style='margin-left:1.5px;'>610</span>",$aux);

      $aux = str_replace("C-410","C-<span style='margin-left:1.5px;'>410</span>",$aux);
      $aux = str_replace("C-510","C-<span style='margin-left:1.5px;'>510</span>",$aux);
      $aux = str_replace("C-610","C-<span style='margin-left:1.5px;'>610</span>",$aux);

      $aux = str_replace("D-410","D-<span style='margin-left:1.5px;'>410</span>",$aux);
      $aux = str_replace("D-510","D-<span style='margin-left:1.5px;'>510</span>",$aux);
      $aux = str_replace("D-610","D-<span style='margin-left:1.5px;'>610</span>",$aux);

      $aux = str_replace("E-410","E-<span style='margin-left:1.5px;'>410</span>",$aux);
      $aux = str_replace("E-510","E-<span style='margin-left:1.5px;'>510</span>",$aux);
      $aux = str_replace("E-610","E-<span style='margin-left:1.5px;'>610</span>",$aux);

      $aux = str_replace("F-410","F-<span style='margin-left:1.5px;'>410</span>",$aux);
      $aux = str_replace("F-510","F-<span style='margin-left:1.5px;'>510</span>",$aux);
      $aux = str_replace("F-610","F-<span style='margin-left:1.5px;'>610</span>",$aux);
      
      return $aux;
    }
}
if (!function_exists('getDefaultImg')) {
  function getDefaultImg(){
    return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAMAAACdt4HsAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyhpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MkU0MDM4NzlGNTIzMTFFNkJBOTlFQUUyNEI5QkE1Q0IiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MkU0MDM4N0FGNTIzMTFFNkJBOTlFQUUyNEI5QkE1Q0IiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyRTQwMzg3N0Y1MjMxMUU2QkE5OUVBRTI0QjlCQTVDQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyRTQwMzg3OEY1MjMxMUU2QkE5OUVBRTI0QjlCQTVDQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgbuPmMAAAEdUExURfT09Pr6+qmpqfn5+aenp/j4+NPT06amptLS0tXV1dHR0aSkpP39/Zubm5qamtnZ2dTU1NDQ0KGhoaKiouzs7M/Pz5ycnJ+fn/Ly8qOjo/v7+93d3dzc3KCgoJ2dnZ6enuTk5Nra2piYmOPj45mZmaWlpbGxsd7e3v7+/tjY2ODg4Ly8vJeXl+/v79vb2/z8/NfX1+np6cbGxr6+vr+/v+Xl5erq6vDw8La2tvHx8eLi4u3t7evr68vLy+fn5+jo6OHh4bq6us3NzcfHx/Pz88PDw6ysrLW1tb29vcjIyObm5sLCwrS0tMzMzLi4uMDAwKurq7m5uc7Ozq+vr+7u7rCwsMTExLe3t8XFxff39/b29tbW1qioqP////X19RMpSskAAALdSURBVHja7NZXe6JAFAbggQGRagvYe++xpCebbHqyvS8Dx///M5a4u0ZwopDc5rvxxvPKA3M+RDx5WZA5e1leCpBX4BXYCJjMPDx5HsAzl7s7h4el0z3MPAMgTKq6HZachMXmEcOjgIDJjKKxqDWPKElnJkaBAMKUYiFrEVEfEBwIYN7oorWcWI43AwDMXjhquaPvMP4BgrMxz7wVjR5g3wD+HhK9gBUrMb4B5w6uzFuhMiE+AcJnw6uAKB7wPgFzFglZlEvYYl4GhP0D6BUgTz0F7Bcw6eegTz0HtLpiqrSTGJmZfgF8RLmC2CF9F2gA4QfSyjaG9nCAPjiSvOuonzGBGqmqu8a3pTbhgwAm7rTbSxcRKaforfpkK2PzPNps//v5XPbitBW01oc3wkm5mc3lcs1c+GPiTWAg87aXbkzGJx/u3u1zBXlXCQ4kuUQ+LctyPC7UhVFQgIdJwbZtzonzURdSEOTNhAjA1o+4vYggbwNQHwMNQDzA/ZWcZu0lIb5fxpBBPgCEQenc5jV5ed5muYLaiM5A2QSgDPDl/YJRd43PCTupXX/tA8zQGkABFGqoBW5l/G/iGnexC2AiOoAU6J9/riXtNUnXEp9KAATRAEiNuVrcXh9W1pJfqrD4w/IIIBgJvTRrbwwrqKoEphdAyoGg+hifE+lK5P+5WgAEphWf846Q51IZD6CM0nXbd1gtBh4A9B4bADBuhqYbaDWNhH+B6/1WiOcpQNnI+72JQmU6xN6nYEK2WPQlsHLlCoZo5RwQ2OE0ezPBJlX9cS9duwCpX5VNN4IValxnqRpcAAKQEpqwlihqk8ulVfBso9MFW5Oi+hTxsNGNLLTQukIBqHbztThHmZdVoyFi1zJTgIc6vB9fF7R8nXNVmqEK3QEGBm18Oz8Qqdz4PZcsqMV4IpHIG0ZR/jaNHrdovY";
  }
}

if (!function_exists('getDefaultLOGO')) {
  function getDefaultLOGO(){
    return "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAARnUlEQVR4Xu2df3Qc1XXHv3d218bUlmZW2DgkJhZJc3JywMZBaGdl49o0DTHGlgQVpS0coHENDaQkDSGQk9jhR05CUtpAEn64J8FJSJqg4p31zwCmphislSKCEzsltAUbu4FEoN2VbSzL2p3b8yQtFUI/3puZnZ2Vds7x8R+678f93s/eN/Nm3nuEKXo1tx+pYeTuF+7nQ+EbttRVvTkVpaCp6HRzKtPIwEMATh/yv4sJ1ydjRmKq6TGlAGh6IaOjD/cBuGrUQDP9JML06daG6vRUAWHKANCYSl+sgTYw8N7xgsug32vg6xOmkZwKEEx6AJrbj3yI7fw9IFyiElAm+nk4n/vsYw2n/ValXLnZTloARLqnE1jHhBsBRBwGJsfA/bl++sq2C/SMwzoCXWzSAdDyKIdOnpm5jkC3AzjNI/W7Cbw+fMh4sPVyyntUZyCqmTQArO3kyBv57F/ZTLcS+MPFUJdBv9WIvz47pP9kQx31F6MNv+ssewBWdb52aig3428BfA7APJ8EPAzgnny491+21J1x3Kc2i9JM2QLQsqcnmtPsGxn4ewA1RVFn4kq7CbgvbGvfKddHx/ICgJma27JLmXAtCC0ATp04Rr5YHAejlRgPJ+L6MyBiX1r1oJGyAKCxo3se5bWroeEaMD7ggd/Fq4LwMmxs5JD9g2R9jRgqAn0FFoCWjq65J+3ICgJfAdDHAGiBVvLdnbMB3smgn07T+ne01s/5fRD7HxgA1jNre1NHzieyVwK4GMBHARS7f+KRLuRDYMSQ8DwY28Dadite9YugDBPFFnhMbZft4nDV9Ow5FKJ6Yl4CwkVgzPYhGIUmrJCdvy2vhV70sc1CU10E/JxBz0Hjjuxb+v6nl1OuBP0o+i9swKfBZ/SjtTZydcRUD1A9A4sIfIrfTjPwHGz7lmRDzR7RdlMqE4Qbtl4QXgDjF0zcoSHcOTs064Afcw2uMsA1uw6c0j2rZiby9iwtb89ECDoxzSegloFaEGphD/wvXsD4kWrH4YleJPBtI1/yBASA0fqdB+N30HAAjAMEHGDxj/gg8sjaIe0YQtrRmqPdxzYurz3h9IekDEBTKvMEgHoAM0sfVCm3X2Om9dMOVz882jRugAGQcm7ISNzLHAPQYZnGx1UKOgFgL4CFKo2UyLYH4Lsj9lvfam2Y1ztWHyYJAAX3fmWZxrkqek86AAj4DRM/2Mf8wx1mzZGJxKgAMJFCI/7elMoEMQOcZNAm1viBzfXGMyouVQBQUWvwrjlIABwEeAPlI99LLJ7VpejKgHkFAEXVAgCAzcCOEPED59QbO24nshVdeId5BQBF9UoEwFFi7GSNtmmh/u2b6ma/rtjtMc0rACgq6R8A/BKItoFoe+TU6t2tZ9NJxa5KmVcAkJLp/42KBQCDToDwNNm8XQvRtk31+iuKXXNkXgFAUTaPAHgd4H0g2g8b+2yN9kePZ/a7mdFSdONt8woAisqpACB+1UT8S2Lex4T9lKd9CIX2J2JV3YrNFs28AoCitCoAAPy4ZUY/odiEr+YVABTlrgCgKJi/5kGbCq5kAH/jjwoAqoJXhgBFxSpDgKJg/ppXMoCq3pUMoKhYJQMoCuaveSUDqOpdyQCKilUygKJg/ppXMoCq3pUMoKhYJQMoCuaveSUDqOpdyQCKilUygKJg/ppXMoCq3pUMoKhYJQMoCuaveSUDqOpdyQCKilUygKJg/ppXMoCq3l5nAAI2MyHu81L3gtsVAEoJABN+PO1V/eqT78t8hEL0VAkg8AWA9qHVwRNqTcCuhGlcOKFhCQ28ygCF4BdWIDfuSYvNL/yGQKwOjqnI6WBxaHY7wCskG9lrmcYiSduSmHkEwCMLY/rVI1cp+Q8B7bBMXWyvI305ACDzIwBXSrXAOGTFjfdL2ZbIyAMARg1+wR2fIXjEMo3Rt8IfQ19lABpTmXtpcHNGmeuoZRpVMoalsnEJwLjB9xsCBu5LmsZNKloqA9CcyqxjQGzELHVle/VIqTZAkumgCwB+tDCmXyO7ONWPTEDA+oRp3CHjd8HGAQDpGxj0HdlGIifDc1qXznpD1t5vO4cAKAXfr0xA4BsTZvS7Kho6AeAKBv2rbCN2KPThzedXvSRr78ZuVeeR01QPf3IAgKPgC79aOtPV/Tl6CsB5bvwcqyyB/zJhRn+qUrcDALJ/xmCxUZTUpcGObzJrUlLGLowa29MXENNWAj6bMI3vy1alAgABP1wQ06+VTfvD+zAU/MIGW7LdU7Ij0McTpv6kSiFlAJpS2fMA7pRthMErk2Z0u6y9E7uh4Is2xM5lTMAaWQhkAQh68Ad1ozrL1J9X0VAdgLbMfBAOyDZCzFcm4tEfy9qr2q3uyCzVbIjg/9GwstIQyABQHsEX6KPWihsHVTRUBmD1s2/M0sLhCXffGtaJz1imca9Kp2Rtm/dk/oQ1bBsR/EJxKQgmBIDxg4Wm/jdBTfvDtbJzuarNS2YfldVvIGeoGBdsm1IZsVuH3EFMjO9ZcWONk3bGK9PUllkGwtYxgi8NwbgAlFHwAfRbpjFNVWdHADSmsq8TeK5MYwT8OmEanm4sORR88cuXOTBi3EwwJgBug99PT4JwvoxGXtiI8w6Tpv4e1bocAdCUyoibQNlHmVzEPlY13m6dKp1encos1zDwy5cJ/oSZYAwANi6M6Z90nPZ9Dv6Qk89bplGnoqWLISD7XYA/JdsY2/biwu7csmVGsxObU2dnDOwdpEz6WE8HowBQjsEXo/n9lqnfoKqvowzQ3Jb+ayZ6RLYxYtyUiBvizF7XV2NbT4zIfhxAtYPK3jUcjADAcfAHziU+gSf8TPvD/Xf6tOUIgFXt2doQs8ouXspvqcYLrpcQFAAg4OEFMX2Nk7Rf6uALrfJEZ22J6dKP5wV9HQEgCqvcCAL8kmVGPT3M0SsIBADlHnynN4CO7wFEwaZU5jEAl0qmYe6Drcvs3i1Z34CZFxAAWOLql98HMfWqfPOl4qeE7SbLNC6TsHuXiZsMcDOBvynbqA1cuNk0dsnay9q5hWBhTA87TvvBCD4Y9Pmkqf+jrGbvuHdwUmjg17enu4E07Tnp8kRfsGL6N6TtFQzdQGCZhvKPYGDMD0jwhUxunrKUnS/EZcV2nj49mhVTwrKzT89apnGBQlyVTJ1CoApA0IIP4GRfWq/acTH1KQk2ZOwYgMH7gOwegOOSDbOWR+2mxcarkvbKZk4gUAEggMEXz/9tlqk3KIvlDQAZMe6IU7ulrsFTu6JflzJ2aKQKgSwAwQz+gEj3WKZxs0O5nL0MKjTWlMpeBvC/STfO2G/FjXOk7R0aqkAgA8BQ8HcqTH877LmTYvTnlqmLJzJHl6sh4NLON95j58KvqbTMNi9INkT3qZRxYisLwUQArNydNSIRFo96su8+nHTXcRktnDvDzQEargAYvA9IpwCSXo1CxHcnYtFbHXusUFAGgvEACHrwAW63zKipIMm7TD0AILsW4IekOyEWi5j6fL8OT54IgrEACH7wheJ0nWXqG6S1H8XQNQArUt1V06GJo9FnyHaEiZcmY9HdsvZu7caDYDQAyiP46O2DPdft7KprAAaHAYXlYmLigunBZFz/O7eBVSk/FgQjARgKvrjhE8fXB/ny5AWbJwAMfaTx7wpqdUfsY/O8+khEtt3RIBgOQBkFH15NrXsCgPhJN7Vn/wfAWbLBIMbnEnHjn2TtvbIbCUEBgHIKPoBXrJj+QS/uo7wBYHAY+DIAlXVpXcdn9J31xMK5b3kVXNl6hkMgACiz4As311mmcaesv+PZeQZAY0f3PLI18U26JtsxZro1GdfvlrX30q4AQX8/1UYiXA5jfsF9mzV7frK+5rAXengGwODNYPpxgFTOr++2c7la1W/ZvXBc1DEEwf1lcMM3zGV+wjKjF3mlgacANLZl/4KIlRYngvFlK27c5ZVDqvVMuDBEtcIi2zPTFcm4/jOvmvEUgKFXxGJqOKrQwSymo9ZaZGQVynhmWmYApPvS+hlOX/2OJpqnAAwMA+2Z+8D4tGKE7rRMY51iGU/MywoAwretmCG7O4uUPp4DcMmeN98b1kL/pbhw42jE1ua3NlSnpXrtoVEZAXA8Z+c/tLXhtN956L6718FjdaQplRGPKF9S6ijzN6x49AtKZTwwLiMA7rJMQzxqe3p5ngFE71p2dc3snxERE0OnK/Q2B5Cpur5dof5RTcsEgD9Eevs/2Lp8zjG3/o4sXxQARCON7dnriPlBtQ7Ti3pv5qN+niJeDgAw0fXJmC7/xlVB9KIB0PIoh/rPzP4awEcU+gNi/HMibvyDShk3tmUAwH9GDukLCjuQuvHVl6eA4Y00ptIXE0gs41a5mGwsTzQY/6FSyKlt0AEo9hY7RcsAhYA0tWV2gvCnigE6aOdyC/yYIQw0AIynrLjxMUXtlMyLD0BH5lzYEBsXSb8jGPLg+5ZpfFLJGwfGAQbAhobzrHpjrwO3pIsUHQDRk6a2zEYQrpbu1ZAhM69OxqNbVMup2AcYgI2WaVyr4osTW18AcDg5JPz5Qz4cOlt180cVIQIKQFEmfXy/CXznDWHmdgKcTPc+OSesr9xQR/0qgZW1DSIADNyRNI31sj64sfMlA4gOru3kSFcus1vlE/KCY+IwhmS9fpUXX8CMFCt4AHD7nLBxQbGAH+m/bwCIhi/pTJ8ZztEvAdQ4oNbVEqix2gsYAOlcmBdtrYsecqCPoyK+AjB4Q5heARqYG3DS9s2WadzjyNMxCgUIANaIV22KRVXnTVzJ4SQIrhoUhZvbsl9l4i86qIiJ+Sovt54NCgB+rpgarntJABiaJhbf4S1zAEE/QJdYpi69Y/l4bQQEgN3ZXv3CUhysURIAREBaOrrmnrSnvSC74+iIIB6ziZZvjunSu5YH+B6gy87lFm1eMltpka2DH86oRUoGwOD9wMB+vyIThJQdIrxBCC1JxKrExyeOrxJnAJuZLkrGdaFBSa6SAiA8bmxPf5GYvurQ+zcJ3Jwwo886LC/WM7DTsq7LMW634sZXXNfjooKSAzC4qqhnK8BK590N81nsXL7GMg1xnJ3yVUIAdi6M6Rc52aFM2clxCpQeAHE/sKcn2q/ZYjyvdeHcXVZMX6c6WVQiAA5TPlyXWDyry4W/nhQNBADCk0ufy7zfDuFpAPOde8Y/03t7rlH5oqgEABxGnpZbi/WXnfvpXcnAAOAVBAxKaflQo+yvy2cADmsaLdtUP7DjeSCuQAEgFBmaLhaZwM1w8Gpes1duqa/5zUQq+wYA41Beo2VONnSeyAc3fw8cAB5CcISI1iRieut4AvkEwKtgLFM90MlNYGXLBhIA0fmh1cYiE0jvOTCG00k7l/vUWBMtPgBwUMtjWTE3yJQN9mh2gQXAYwiOMNEtyfrqDSOfEooMwIFcmJf5+XZPFYZAA/A2BKztAuMDqs6NtCfgmXwotHb4UbZFBOCVXJiXBzn4Qp/AAzDwdJDqfp9N2tNeQABCH4PvOD1kfFN8dFEUAAgvM9nLvdrEwS3445UvCwDehgCa+ED0XC8EEcfZkaatsW27w4v6htWxV4O9apNZ878e11uU6soGAOG9ODWsZ0b1txh0nUdq5B29iBqjcQI/VN3b8xmViSiP/HBcTVkBUPCyOZUWR9iLHTJnOfbc24JHCbxW9eh2b7vgrLayBEC4elmq54/zsB/1akhwJt9Aqb0haJc/Zlb/t4s6Sla0bAEo0pCgFIhyTPmjPBkp+RxI4xIMCWWb8iclAD4PCWWd8ictAH4MCZMh5U9qAArOre7ILNVsiDMN6z0aszpsDZ/fXG8841F9gammrG8CJ1KxqS17OYi/5uKF0itgus2K6+JpY1JekxoAEbGW/Tyt/1hWnE0gdtiSXZLWDeDOyEz9gdazSXxzOGmvSQ9AIXItnenqXB632azdROBTRosog05oZN8bDuFrrXXRnkkb9WGOTRkACj6L7ww0W7uLgSuH7VpiE/CIrdlfKocXOF6COeUAKIjX3J5ZyMwDZxkT0S2JmPErL4Utl7r+DwFI+OqVvFtOAAAAAElFTkSuQmCC";
  }
}


if(! function_exists('contarPalabras')){
    function contarPalabras($te, $ti){
        $te = preg_replace('/\n/', '\n', (String)$te);$te = explode('\n',$te);$cC = 0;$tiC = 0;$ce = '¡ ! — • ( ) ? ¿ % * @ # ° | ¬ $ % & / ´ ¨ { } [ ] ^ ` ÷ + ≠ \' " , : ; ... .';$ce = explode(' ',$ce);$teAux = str_replace($ce, ' ', $te);foreach($teAux as $x){if($x != ''){foreach(explode(' ',$x) as $y){if($y!= '') $cC++;}}}$tiAux = str_replace($ce, ' ', $ti);foreach(explode(' ',$tiAux) as $x) $x!= '' ? $tiC++ : false ;$cS = 0;$tiS = 0;$ce = '¡ ! — • ( ) ? ¿ % * @ # ° | ¬ $ % & / ´ ¨ { } [ ] ^ ` ÷ + ≠ \' "';$ce = explode(' ',$ce);$teAux = str_replace($ce, ' ', $te);foreach($teAux as $x){if($x != '') foreach(extraerPalabras($x)[0] as $j)$j != '' ? $cS++ : false;}$tiAux = str_replace($ce, ' ', $ti);foreach(extraerPalabras($tiAux)[0] as $x) $x != '' ? $tiS++: false;
        return [$cS,$cC,0,0,$cC+$tiC,$cS+$tiS];
    }
}
if(! function_exists('extraerPalabras')){     
    function extraerPalabras($te){
        $te = preg_replace('/\n/', ' ', (String)$te);$te = preg_replace('/\t/', ' ', (String)$te);$ca = ', : ;';$ca = explode(' ',$ca);$cC = $te;for($i = 0; $i < count($ca); $i++){$cC = str_replace($ca[$i], ' '.$ca[$i].' ', (String)$cC);}$cC = str_replace('...','¥',$cC);$cC = str_replace('.',' . ',$cC);$cC = str_replace('¥',' ... ',$cC);$cs = str_replace($ca, '  ', (String)$te);$cs = explode(' ',$cs);$cC = explode(' ',$cC);for($i = count($cC)-1; $i > -1 ; $i--) if(!ctype_print(strtolower(quitarAcentos($cC[$i])))) array_splice($cC,$i,1);for($i = count($cs)-1; $i > -1 ; $i--) if(!ctype_print(strtolower(quitarAcentos($cs[$i])))) array_splice($cs,$i,1);        
        return [$cC, $cs];
    }
}

if(!function_exists('imgEstudianteDefault')){
  function imgEstudianteDefault(){
    return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACBCAYAAAAIYrJuAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADOpJREFUeNrsXetR40gQnnHt//NFcCKC1UaAiAA7AnAE4AiACMARABHgjQARASICvBGcLoI7tem5HYvRo+ctoalyGQojz/T39XNejE3tSzf+lQf/9PSUVG8J/rpbLpe7iQDjAjarXn9UrxT/lBEfleN7Ub3+wd9HRRQ+EsAzBPcYwZ47/soSSfECpKgIkU8E8K/hi+p1qqHVrtoWCbEdkoXgAwT9TDLpsTawDo9DIAMfAPAC9MVArSxYhseKCNuJAP1BnyPgV1KUPvQGluCmIsLDRIB24C+r14WHQC5UgwByU73uKjKUEwF+g39evd2OGPgoLQKPAHiI4u9HZOp1iLAKlUrygMDPEfgFm5oIFle+3QIPBH4Icy+KNyJN+6fls8f4nni2TCWSYDtKAnjU+hxBfmMfpdvcsN8pEiFFcmRjsQbcI/ggtCdHWr9Dof305Uul8vMpc1OYgjFVw1kWgydAJaxLNPkuQN+ErrY5rlKuXGYK3INwwOSfW3wkCOMx1gkYdBcXtsdcjXc1KAKgv3+2pBElAr8ZykSLg6JWji6hjJ4AlsG/w4JJyQbYLBMB4oETm7LgDgacYqRvCj749/VYFl9gnHBlwTVYJQF3AP6zIdN3LGBlzFP2YFr5tEYCbnFgAPqr4cAGbe6J8rpGixCUBNwi+CY+f9Ra79AaGJNgFgH4APqPrwY++wjp92NnvxefUptwueEsgGGeD3PiazY1kOMtZgte6wQzw05fG4C/msA/sAYgC91izzlWW/1ZAPRfOuanxIJGPsGulCsolO5M6QlVrlyzk9C5d41OltjJYoLaSToN8j2iBIW6LuBpAt+pO9hH9ygzShPT7e5iAPQ1maZ5msCnkUAnRlpQ4gFOBB/y1VcN7V/FthxaGg+8yljJiTHBPfHfSkytd7YJ8Kyh/VDPv4sMdNXcPQhLbOIoIiOBTooIu5KW1gigyURn89iWQG9q0ZFBU/mWXesLec8v14n6rU9dOgY9ajJozrXAesgjGwS4ZrSJiyARv4cNpEHJoFl7gcm1a20CaGq/N78fcNdwEDJoKmNjbaAPAai1fjgw4WSkoEdBhmr8r8RxN1oB3kPQ78T+HblYxYPVMbFNPGHxNudk0HAFjVaAWzY3rf5mxKB7J4OGZVZiwy36fnIdeqSgd5HhxcbWLx18qu/9k0IAat6vXe0bMehtyiJ2Mm0NSEC10J8waiPAOwGMznxzAt0+GTSswKcAnbeA82pb+yfQ7ZNBwwocBOnfGj50QfFtbeBPoJPaHAM7WOHTlwx3jLbpBD677iIAZfv24wR6ODJA0F39fUvICBYyAWYK8BaMVvW7U+SoYvZqAt8uGW4b5vpvCM9KUEHVBGAf+937tm097YM1aRho/MCOTYtAzOMCUDKY3z9SldjRp1PkfNbmAjIT8y91qsBOXTs6H2AKBg/bhpC2Z8osgBj9KwsLLcGgqF8DKXI0afMJazu1AUwJ/6ZmA98MtJ/ayZ9IgBLXwK8x3jjFwGQ+ga7fMBjMCRjC5x7qBDgmAkodbL3TWxTACq1Phn0Y89FxO2axJKzApC8BjlUEoEwx5hoDZ02ZgRQz3I30DEGQ143jDTFbQqyVHmQBtetTulqhMemTthGgbs5wQePDWMCHzMj1bijMBnZaBCDm7DoD+V4jW582lr2DG4/f1TsdhJrNTDMAfNPoWEZ1NWhlxlBH8Lkw9oVilWUC/OWCZci0epR/zKYW3AIA5louQGN1y1ntd0pwl06YOiPAgQVIHWn/XAH4QT265X8n8DXqAQSXM5cJQFn6RWmLHjHB1MJYgQ8LgFrqIsho8/enPdhceA6gXLWd5+8jnw/g0tQmhhZgM3TwAxx2+UYlgMuWteWhPazANfuYVh6iJQALtoy5gzoEyAkBYKJLDgUJYNEp7DR+iLg2AJq+RcLC/P2P2A/F+Ob4+V0E+N6DRBkGqDmuPXyo/U3cFXyM76knoHdIxF/4flAeh7gKl9YXAUgASnoVAwFSQ4IIK3GFQs2ZdGEz1tbzhtRTdWP4H6z/+QC/FFav1Z9LKe+plP1sY3YDrgkwNyRIPVvIBCEqYQtgPmkhamJOdVkmrWX/fgp/i/X841mszEQT2nUELZBBLDeDz74TJppcFGBUJ3tBf56JqfZoCGBS839itGIRaPpRyPsF8LtVJEhxPL5ab7LpuIDMtVmVbuSq++BEYWLB9G/kzSnS5Qzis486+xZ1ngMBX/V/R2iVEilIzWBcnk5ITV0SwEeTJ48+LaRAgiiPdpN8sawFIPyEsnW95TmsBwlKTFnrscGFr5iE6gJcmc2usnHRkR0oL33EDKDpf5sWmJ5paJHqOZRtc4IMj1Lf4osBiH7T5lz+rqNApEPMxCDlVPXB9Dkq9+a6HVMtgJMAo4e5+6kJgo5VoRZjcs0x2ZSf6+8oZxoDS4lgtOW/2x4aM9cATmU5KPvnWEORqaQ+x1B+rr+jmNUG19eMUdb0naDQxEsItu0SxFJXYPhM2Je4xu9bY3pIXoOPexxXUt91a/vHBpZIJ3Pp20o5C3gjBClJ34FI6/2pZjzTTTuRBFbOKTQ95BotWKajaB4szNusR0DmOhDs8tenbNhtYRiLuCTAgQugdMx1JCunj6mnyNlVk9PGrYc5AYpyFvXdwf8S/vlPl4OpHVIlCisl5vOJ69NIDVK8CyRwgdoob9Vyfm8CAcP97u6ZQYrjWitvamkN1NJhwuecYWUvUm1fsN+TUzL4uQfwKYWmQlUHoCz4dOqbUVhtgdxVZNqftATRvpaGUcz/i4oAFAvgvKyJ5wgcYSpXD1IXkU2xntd+32G/wVX5ujeBgskea65g8t+sfyVp6WCPe1s6dVsTtNWziQ37Jh/Y6P2mFN3TXWZNzIjBDdRzexSqTLiLSKzApQS+0PyQ2UYv7W8iAOXkjxBmeC0VU+ZM/75dm9p/UbNKZYA+UMz/zzYCUEw69YttWALQsE1EVuBA+wNdj0c9Y2nbSABkL4UEIaLxu5oVuA2k/Ult/I+BSEjB4KAYNesyET1a4rtShwOQ6wTngaqF97Wo/y4ACWHclJrIAUlnLTl4GbMVwBMz5fL1vU9XoLhCdxVo6TfpAql61ta2IITiy7JAGiinWgmjX2xpknLJbufB02JPlfZT5P4J0zYCUHfl3gawAkXNFSzwmlXXEfdzzfSHOsyKOtZNbwJgtE0JBlPcC+ebBNe12sWlq35I4AtXU7L2hS0uiQhjpEz9PqjWfs6ojOliZKCUbKmIB84dgS8LfR3oBlGdzEeZocw6tCtntMrgPFBAWCIJyhoJ7i0JXJRZ01rQFyLnF4EfaXFuU4zSZ1UwdRHkZYiAsGFbFqSHryaHTWG0/1xLtYKBj7KlVj8bMex7eTT16nIA40cg36gy1Qxz9E3fPRAo6CvFuEOCr3ODeOvEVN+tYfAAyhWyIiVbBrAEcGz6Cfs8c3iJ1ml/PDuaxZ0CdCDOhULIOwz4Qp74cc/oG1NaLTgnsO9Ww/SE9JNihcwtM7+7CKzHTcg9/pq3rnROl3Oi+aFeI8+Y/lp62ynTlQYRHlCIu8D9zxjtsujebphraBR1n3uJHQkqREmQp2jmswahFegitjGc6oEB7LOG4vVarMM1OgQEoE4Bg1BPYj0mJdbWEtB2tS3et9DZdDaHrhh9d8uexbEekzIy8HfscI7ELgGkogubSBAd+CLwLp0RAEmQM72574kEbsEn30nEDTtLLRBNMUF3wHevCX5vv29sAWRjwPRO8YABvk/3ASij/VRTobSWoRsRoGESpm+bozs4n8Dfy+CV6Z0eUjKD1UjcIntfDR5xh7uAvqK/r5esqeCfmBTauGUWm0y/Fsjk4ouAnzG92n7NCJvtzOKWB2VKAhHJXo9c66Esbbqhxco8C3cwQBsk2OEA85GBb2tyytokG3c0UBsk2Kc27GPZ1W7gwGdMvbZAx+evbc6wcseDfmJ2zsV7YBHMygUE3krA55UAtdzWVuUPLMImdteApv6C2TtFxVmAzD0Iw6S02RYjwIrlbSxWAfcJiiNiEouPzpnDpefco4B0VhT11Y7HEGRACyeuiHFR1XSeEXHPAltgcOhqMmiHGrM/pcu2yZTWDB6jeXc1DrHhxLmr80oAyVTeM39Xx+Yo0Dd8LxSkmSvAFP3zeRuZiHO8bTT1TgCJCJeMvsFhzE3U9Lc+vzTYpVG4vfuI0XYhj7XtZeEb/KAWwGG+PKQG7mkdcv6DxyQNi6XSIQB/E0M9g8coHSwln43QIkQDfNQEqLkGIML5wIGHOGcT41Q3H4L0sJoorMJQlpHt7zNkkWwwGTQBamRwXX0zBT1IVfLLEKBGhgTjBFGZSwIADq+X2DV9lARoIESKr+/s99WttgI40Opf+HMxhmXtoyJARwwhiNCnrPt/lD62VUn19p8AAwANmCVtEgqRJwAAAABJRU5ErkJggg==';
  }
}

if (!function_exists('topRanking')) {
  function topRanking($array,$corte){
    $alumnos = $array;
    //dd($alumnos);
    $array = [];
    foreach ($alumnos as $cont => $alumno) {
      //dd($alumno);
        $puntos = $alumno['PUNTOS'];
        $estrellas = $alumno['ESTRELLAS'];
        $nivelAlumno = NivelAlumno($estrellas,$puntos);
        $datosAlumno = [
            "_id" => $alumno['_id'],
            'COLEGIO' => isset($alumno['COLEGIO'])?$alumno['COLEGIO']:null,
            "CURSO" => $alumno['CURSO'],
            'ALUMNO' => $alumno['ALUMNO'],
            'NIVELNOMBRE' => $nivelAlumno['NIVEL']['NOMBRE'],
            "IMAGEN" => $alumno['IMAGEN'],
            'ORDEN' => $nivelAlumno['NIVEL']['ORDEN'], 
            'PUNTOS' => $puntos, 
            'ESTRELLAS' => $estrellas, 
            'NIVEL' => $nivelAlumno['NIVEL'],
            'PORCENTAJE' => $nivelAlumno['PORCENTAJETOTAL'],
            //"CATEGORIAEPM" => $alumno['CATEGORIAEPM'],
            //"EPM" => $alumno['EPM'],
            'NUMEROCHANTA' => $nivelAlumno['NIVEL']['ORDEN']+(($nivelAlumno['PORCENTAJETOTAL']/100))
            ];
          if (isset($alumno['CATEGORIAEPM'])) {
            $datosAlumno['CATEGORIAEPM'] = $alumno['CATEGORIAEPM'];
            $datosAlumno['EPM'] = $alumno['EPM'];
          }
        array_push($array, $datosAlumno);
    }
    $array = array_orderby($array,'NUMEROCHANTA',SORT_DESC);
    $return=array_slice($array,0,$corte);
    $top=array_search(session("idAlumnoActivo"), array_column($return, "_id"));      
    if($top===false){$return=array_slice($return,0,$corte-1);
      $pos=array_search(session("idAlumnoActivo"), array_column($array, "_id"));              
      $pos!==false?$return[$pos]=$array[$pos]:$return=array_slice($array,0,$corte);
    }
    foreach($return as $pos=>$x){      
      $img=\App\Models\MTPuntajeNacional::where("PROMOCION",session("PROMOCION"))->where("IDALUMNO",newMongoId((String)$x["_id"]))->first();
      $coso = \App\Models\Alumnos::find((String)$x["_id"]);
      $return[$pos]["SEXO"] = $coso->SEXO;
      !empty($img)?$return[$pos]["IMAGEN"]=$img->IMAGEN:"";
    }    
    //dd($return);
    return $return;
  }
}
if(!function_exists('getImgComenzar')){
  function getImgComenzar(){
    return asset('assets/juegos/img/btn_play.png');
  }
}