<?php

namespace App\Models;

use Moloquent;

class PRODUCTOS extends Moloquent
{
   protected $connection = 'data';
   protected $table = 'PRODUCTOS';  
   protected $fillable = [
	'CODIGO', 'DETALLE', 'NOMBRE','STOCK'
   ];

	public static function modify($id, $DATA){		
		$User = PRODUCTOS::find($id);
		$User->CODIGO = $DATA["CODIGO"];
		$User->DETALLE = $DATA["DETALLE"];
		$User->NOMBRE = $DATA["NOMBRE"];
		$User->STOCK = $DATA["STOCK"];
		return $User->save();
	}
}
