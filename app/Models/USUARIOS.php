<?php

namespace App\Models;

use Moloquent;

class USUARIOS extends Moloquent
{
   protected $connection = 'data';
   protected $table = 'USUARIOS';  
   protected $fillable = [
	'RUN', 'PASS', 'NAME'
   ];

	public static function auth($DATA){		
		return USUARIOS::raw()->aggregate([
		    ['$match'=>[
					"RUN"=> $DATA["RUN"], 
					"PASS"=> $DATA["PASS"]
				]
			],
	 	])->toArray();
	}

	public static function modify($id, $DATA){		
		$User = USUARIOS::find($id);
		$User->RUN = $DATA["RUN"];
		$User->NAME = $DATA["NAME"];
		$User->PASS = $DATA["PASS"];
		return $User->save();
	}
}
