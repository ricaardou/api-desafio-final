<?php

namespace App\Models;

use Moloquent;

class Usuario extends Moloquent
{
   protected $connection = 'data';
   protected $table = 'USUARIO';  

	public static function verificarfecha($run){		
		$ACCESO=Acceso::raw()->aggregate([
		    ['$match'=>[
					"RUN"                         => $run, 

				]
			],
	 	])->toArray();
        return $ACCESO;
	}
	
}
